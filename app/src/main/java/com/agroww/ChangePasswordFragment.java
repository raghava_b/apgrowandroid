package com.agroww;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by raghavakumarburugadda on 28/05/17.
 */

public class ChangePasswordFragment extends Fragment{

    View root_view;

    @BindView(R.id.old_password_edittext)
    EditText old_password_edittext;
    @BindView(R.id.confirmpassword_edittext)
    EditText confirmpassword_edittext;
    @BindView(R.id.password_edittext)
    EditText password_edittext;
    @BindView(R.id.update_password)
    Button update_password;

    public static ChangePasswordFragment newInstance(Bundle bundle) {
        ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
        changePasswordFragment.setArguments(bundle);
        return changePasswordFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        root_view = inflater.inflate(R.layout.fragment_changepassword, null);
        ButterKnife.bind(this, root_view);
        return root_view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
