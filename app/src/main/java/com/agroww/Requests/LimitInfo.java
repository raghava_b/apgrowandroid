package com.agroww.Requests;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class LimitInfo {

    public int limit_start;
    public int limit;

    public int getLimit_start() {
        return limit_start;
    }

    public void setLimit_start(int limit_start) {
        this.limit_start = limit_start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
