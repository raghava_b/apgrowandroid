package com.agroww.Requests;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class LimitInfoForBalanceSheet {
    public String limit_start;
    public String limit;
    public String user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLimit_start() {
        return limit_start;
    }

    public void setLimit_start(String limit_start) {
        this.limit_start = limit_start;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }
}
