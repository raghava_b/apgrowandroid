package com.agroww.Requests.AddUserBalancesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by raghavakumarburugadda on 04/06/17.
 */

public class AddUserBalanceSheetRequest {
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("method_id")
    @Expose
    private Integer methodId;
    @SerializedName("method_name")
    @Expose
    private String methodName;
    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("season_id")
    @Expose
    private Integer seasonId;
    @SerializedName("season_name")
    @Expose
    private String seasonName;
    @SerializedName("crop_variety_id")
    @Expose
    private Integer cropVarietyId;
    @SerializedName("crop_variety_name")
    @Expose
    private String cropVarietyName;
    @SerializedName("crop_size")
    @Expose
    private String cropSize;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("balance_sheet")
    @Expose
    private List<UserBalanceSheet> balanceSheet = null;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMethodId() {
        return methodId;
    }

    public void setMethodId(Integer methodId) {
        this.methodId = methodId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public Integer getCropVarietyId() {
        return cropVarietyId;
    }

    public void setCropVarietyId(Integer cropVarietyId) {
        this.cropVarietyId = cropVarietyId;
    }

    public String getCropVarietyName() {
        return cropVarietyName;
    }

    public void setCropVarietyName(String cropVarietyName) {
        this.cropVarietyName = cropVarietyName;
    }

    public String getCropSize() {
        return cropSize;
    }

    public void setCropSize(String cropSize) {
        this.cropSize = cropSize;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<UserBalanceSheet> getBalanceSheet() {
        return balanceSheet;
    }

    public void setBalanceSheet(List<UserBalanceSheet> balanceSheet) {
        this.balanceSheet = balanceSheet;
    }

}
