package com.agroww.Requests.AddUserBalancesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by raghavakumarburugadda on 04/06/17.
 */

public class UserBalanceSheet {
    @SerializedName("expense_cat_mst_id")
    @Expose
    private Integer expenseCatMstId;
    @SerializedName("expense_item_id")
    @Expose
    private Integer expenseItemId;
    @SerializedName("expense_item_val")
    @Expose
    private Integer expenseItemVal;
    @SerializedName("iteration")
    @Expose
    private Integer iteration;
    @SerializedName("expense_date")
    @Expose
    private String expenseDate;

    public Integer getExpenseCatMstId() {
        return expenseCatMstId;
    }

    public void setExpenseCatMstId(Integer expenseCatMstId) {
        this.expenseCatMstId = expenseCatMstId;
    }

    public Integer getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(Integer expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    public Integer getExpenseItemVal() {
        return expenseItemVal;
    }

    public void setExpenseItemVal(Integer expenseItemVal) {
        this.expenseItemVal = expenseItemVal;
    }

    public Integer getIteration() {
        return iteration;
    }

    public void setIteration(Integer iteration) {
        this.iteration = iteration;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

}
