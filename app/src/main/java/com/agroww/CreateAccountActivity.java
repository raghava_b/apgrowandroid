package com.agroww;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agroww.Responses.LoginResponse.LoginResponse;
import com.agroww.localDBUtils.localLoginDetails.LocalLoginData;
import com.agroww.localDBUtils.localLoginDetails.LocalLoginFactory;
import com.agroww.networkutils.VolleyCallback;
import com.agroww.networkutils.VolleyUtilityClass;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmEmail;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import apcare.apgrowcomponents.ApgrowPopupView;
import apcare.apgrowcomponents.DatePickerUtils;
import apcare.apgrowcomponents.ICardClick;
import apcare.apgrowcomponents.TestActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import io.realm.RealmResults;

public class CreateAccountActivity extends BaseActivity implements Validator.ValidationListener {

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.firstname_edittext)
    EditText firstname_edittext;

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.lastname_edittext)
    EditText lastname_edittext;

    @Length(min = 10, max = 10)
    @NotEmpty
    @BindView(R.id.mobile_edittext)
    EditText mobile_edittext;


    @Password(min = 6, scheme = Password.Scheme.ALPHA)
    @NotEmpty
    @BindView(R.id.password_edittext)
    EditText password_edittext;

    @ConfirmPassword
    @BindView(R.id.confirmpassword_edittext)
    EditText confirmpassword_edittext;

    @NotEmpty
    @BindView(R.id.dob_edittext)
    EditText dob_edittext;

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.address_edittext)
    EditText address_edittext;

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.village_edittext)
    EditText village_edittext;

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.mandal_edittext)
    EditText mandal_edittext;

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.district_edittext)
    EditText district_edittext;

    @Email
    @NotEmpty
    @BindView(R.id.email_edittext)
    EditText email_edittext;

    @BindView(R.id.create_account)
    Button create_account;
    @BindView(R.id.gender_layout)
    RelativeLayout gender_layout;
    @BindView(R.id.country_layout)
    RelativeLayout country_layout;
    @BindView(R.id.state_layout)
    RelativeLayout state_layout;

    @BindView(R.id.activity_create_account_layout)
    LinearLayout activity_create_account_layout;

    @BindView(R.id.genderText)
    TextView genderText;
    @BindView(R.id.country_text)
    TextView country_text;
    @BindView(R.id.state_text)
    TextView state_text;

    Validator validator;
    private static final String TAG = "CreateAccountActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        DatePickerUtils datePickerUtils = new DatePickerUtils(dob_edittext, CreateAccountActivity.this);
    }

    @OnTouch(R.id.left_arrow)
    public boolean onBackarrowClicked() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {

    }

    @OnClick(R.id.country_layout)
    public void onCountryClicked() {
        setupCountries();
    }

    @OnClick(R.id.gender_layout)
    public void onGenderClicked() {
        setupGender();
    }

    @OnClick(R.id.state_layout)
    public void onStateClicked() {
        setupStates();
    }

    @OnClick(R.id.create_account)
    public void createAccount() {
        validator.validate();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_create_account;
    }

    @Override
    public String getToolbarName() {
        return "Registration";
    }

    private void setupCountries() {
        final ArrayList<String> countries = new ArrayList<>();
        countries.add("INDIA");
        countries.add("USA");
        countries.add("UK");
        countries.add("SPAIN");
        countries.add("ISTAMBUL");
        countries.add("TURKEY");

        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text,int position) {
                Log.e("", "clickedPosition: " + text);
                country_text.setText(text);
            }
        };

        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(CreateAccountActivity.this, "Countries", countries, iCardClick, activity_create_account_layout);
        apgrowPopupView.initView();
    }

    private void setupGender() {
        final ArrayList<String> gender = new ArrayList<>();
        gender.add("Male");
        gender.add("Female");
        gender.add("Other");
        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text,int position) {
                Log.e("", "clickedPosition: " + text);
                genderText.setText(text);
            }
        };

        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(CreateAccountActivity.this, "Gender", gender, iCardClick, activity_create_account_layout);
        apgrowPopupView.initView();
    }

    private void setupStates() {
        final ArrayList<String> states = new ArrayList<>();
        states.add("Andhra Pradesh");
        states.add("Tamil Nadu");
        states.add("Telangana");
        states.add("Kerala");
        states.add("Karnataka");
        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text,int position) {
                Log.e("", "clickedPosition: " + text);
                state_text.setText(text);
            }
        };

        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(CreateAccountActivity.this, "States", states, iCardClick, activity_create_account_layout);
        apgrowPopupView.initView();
    }

    @Override
    public void onValidationSucceeded() {

        callRegisterAPI();

    }
    private void callRegisterAPI() {
        VolleyCallback volleyCallback = new VolleyCallback() {
            @Override
            public void volleyResponse(String string) {
                Log.e(TAG, "volleyResponse: " + string);
                if (!checkIsNullorNot(string)) {
                    parseandSaveLoginAPI(string);
                } else {
                    showAlertDialog(getString(R.string.alert), getString(R.string.networkerror));
                }

            }
        };
        try {
           /* {
                "first_name":"nagendra",
                    "last_name":"immadi",
                    "middle_name":"",
                    "mobile":"1234567890",
                    "password":"123121",
                    "email":"na@gmail.com",
                    "dob":"1988-10-18",
                    "country":"india",
                    "state":"telangana",
                    "address":"",
                    "district":"hyderabad",
                    "mandal":"rajendra nagar",
                    "taluk":"",
                    "village": "manikonda",
                    "gender":"1"
            }*/
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("first_name", firstname_edittext.getText().toString());
            jsonObject.put("last_name", lastname_edittext.getText().toString());
            jsonObject.put("middle_name", "");
            jsonObject.put("mobile", mobile_edittext.getText().toString());
            jsonObject.put("password", password_edittext.getText().toString());
            jsonObject.put("email", email_edittext.getText().toString());
            jsonObject.put("dob", dob_edittext.getText().toString());
            jsonObject.put("country", country_text.getText().toString());
            jsonObject.put("state", state_text.getText().toString());
            jsonObject.put("address", address_edittext.getText().toString());
            jsonObject.put("district", district_edittext.getText().toString());
            jsonObject.put("mandal", mandal_edittext.getText().toString());
            jsonObject.put("taluk", mandal_edittext.getText().toString());
            jsonObject.put("village", village_edittext.getText().toString());
            if(genderText.getText().toString().equals("Male"))
            jsonObject.put("gender", "1");
            else
                jsonObject.put("gender", "0");
            String url = getString(R.string.base_url) + "registerNewUser";
            VolleyUtilityClass volleyUtilityClass = VolleyUtilityClass.getInstance(CreateAccountActivity.this);
            volleyUtilityClass.callPOSTAPI(url, jsonObject, volleyCallback);
        } catch (Exception e) {

        }
    }

    private void parseandSaveLoginAPI(String response) {

        try {
            Log.d("::RESPONSE::","::"+response);
            Toast.makeText(this, "Yay! we got it right!", Toast.LENGTH_SHORT).show();
//            showAlertDialog("Success", "Your registration Successful");
            finish();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
