package com.agroww.networkutils;

public class LruBitmapCache extends android.support.v4.util.LruCache<String, android.graphics.Bitmap> implements
		com.android.volley.toolbox.ImageLoader.ImageCache {
	public static int getDefaultLruCacheSize() {
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 8;

		return cacheSize;
	}

	public LruBitmapCache() {
		this(getDefaultLruCacheSize());
	}

	public LruBitmapCache(int sizeInKiloBytes) {
		super(sizeInKiloBytes);
	}

	@Override
	protected int sizeOf(String key, android.graphics.Bitmap value) {
		return value.getRowBytes() * value.getHeight() / 1024;
	}

	@Override
	public android.graphics.Bitmap getBitmap(String url) {
		return get(url);
	}

	@Override
	public void putBitmap(String url, android.graphics.Bitmap bitmap) {
		put(url, bitmap);
	}
}