package com.agroww.networkutils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by raghavakumarburugadda on 26/01/17.
 */
public class VolleyUtilityClass {
    private static VolleyUtilityClass ourInstance = new VolleyUtilityClass();
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mContext;
    private static final String TAG = "VolleyUtilityClass";
    ProgressDialog progressDialog;
    JsonObjectRequest jsonobjectRequest;
    StringRequest stringRequest;

    public static VolleyUtilityClass getInstance(Context context) {
        mContext = context;
        return ourInstance;
    }

    public void callPOSTAPI(String url, JSONObject post_params, final VolleyCallback _volleycallback) {

        Response.Listener<JSONObject> response_listener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                _volleycallback.volleyResponse(response.toString());
                //Log.e(TAG, "onResponse: "+response.toString());
            }
        };
        Response.ErrorListener error_listener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _volleycallback.volleyResponse("volleyerror");
            }
        };
        Log.e(TAG, "post_param: " + post_params.toString());
        jsonobjectRequest = new JsonObjectRequest(Request.Method.POST, url, post_params, response_listener, error_listener);
        getRequestQueue().add(jsonobjectRequest);
    }
    public void callPUTAPI(String url, JSONObject post_params, final VolleyCallback _volleycallback) {

        Response.Listener<JSONObject> response_listener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                _volleycallback.volleyResponse(response.toString());
                //Log.e(TAG, "onResponse: "+response.toString());
            }
        };
        Response.ErrorListener error_listener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _volleycallback.volleyResponse("volleyerror");
            }
        };
        Log.e(TAG, "post_param: " + post_params.toString());
        jsonobjectRequest = new JsonObjectRequest(Request.Method.PUT, url, post_params, response_listener, error_listener);
        getRequestQueue().add(jsonobjectRequest);
    }

    public void callGETAPI(String url, final VolleyCallback _volleycallback) {
        showProgressDialog(mContext);
        Response.Listener<String> response_listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                _volleycallback.volleyResponse(response);
            }
        };
        Response.ErrorListener error_listener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                _volleycallback.volleyResponse("volleyerror");
            }
        };
        stringRequest = new StringRequest(Request.Method.GET, url, response_listener, error_listener);
        getRequestQueue().add(stringRequest);
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }

        return mRequestQueue;
    }

    public void showProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context, AlertDialog.THEME_HOLO_LIGHT);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideProgressDialog() {
        if (progressDialog != null || progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    private VolleyUtilityClass() {
    }
}
