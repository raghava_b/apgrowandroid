package com.agroww.networkutils;

/**
 * Created by raghavakumarburugadda on 26/01/17.
 */

public interface VolleyCallback {
    void volleyResponse(String string);
}
