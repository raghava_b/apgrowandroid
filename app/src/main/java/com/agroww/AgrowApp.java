package com.agroww;

import android.app.Application;
import android.content.SharedPreferences;

import com.agroww.Responses.LoginResponse.LoginResponse;
import com.agroww.dtos.FarmingData;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;


/**
 * Created by raghavakumarburugadda on 28/05/17.
 */

public class AgrowApp extends Application {

    public static AgrowApp agrowApp;
    public ArrayList<FarmingData> farmingDatas = new ArrayList<>();
    public SharedPreferences sharedPreferences;
    LoginResponse loginResponse;
    public ArrayList<FarmingData> getFarmingDatas() {
        return farmingDatas;
    }

    public void setFarmingDatas(ArrayList<FarmingData> farmingDatas) {
        this.farmingDatas = farmingDatas;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        agrowApp = this;
        Realm.init(this);
    }

    public static AgrowApp getInstance() {
        if (agrowApp == null) {
            agrowApp = new AgrowApp();
        }
        return agrowApp;
    }

}
