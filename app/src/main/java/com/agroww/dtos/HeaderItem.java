package com.agroww.dtos;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.List;
import com.agroww.dtos.ChildItem;
import com.agroww.dtos.*;

/**
 * Created by raghavakumarburugadda on 27/05/17.
 */

public class HeaderItem implements Parent<ChildItem> {
    int view_type;
    String headername;
    String totalvalue;
    List<ChildItem> childItems;
    public void setChildItems(List<ChildItem> childItems) {
        this.childItems = childItems;
    }



    public int getView_type() {
        return view_type;
    }

    public void setView_type(int view_type) {
        this.view_type = view_type;
    }

    public String getHeadername() {
        return headername;
    }

    public void setHeadername(String headername) {
        this.headername = headername;
    }

    public String getTotalvalue() {
        return totalvalue;
    }

    public void setTotalvalue(String totalvalue) {
        this.totalvalue = totalvalue;
    }

    @Override
    public List<ChildItem> getChildList() {
        return childItems;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
