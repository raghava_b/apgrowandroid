package com.agroww.dtos;

import com.agroww.Responses.ExpenseItems.ExpenseItem;
import com.agroww.localDBUtils.localExpenseItems.LocalExpenseItems;
import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.List;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class NewHeaderItem implements Parent<LocalExpenseItems>{

    String headername;
    String header_categoryid;
    int totalvalue;
    List<LocalExpenseItems> childItems;

    public String getHeader_categoryid() {
        return header_categoryid;
    }

    public void setHeader_categoryid(String header_categoryid) {
        this.header_categoryid = header_categoryid;
    }

    public String getHeadername() {
        return headername;
    }

    public void setHeadername(String headername) {
        this.headername = headername;
    }

    public int getTotalvalue() {
        return totalvalue;
    }

    public void setTotalvalue(int totalvalue) {
        this.totalvalue = totalvalue;
    }

    public void setChildItems(List<LocalExpenseItems> childItems) {
        this.childItems = childItems;
    }

    @Override
    public List<LocalExpenseItems> getChildList() {
        return childItems;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return true;
    }
}
