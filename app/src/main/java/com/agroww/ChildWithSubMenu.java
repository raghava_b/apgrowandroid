package com.agroww;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;


/**
 * Created by raghavakumarburugadda on 27/05/17.
 */

public class ChildWithSubMenu extends ChildViewHolder {
    TextView fieldname,fieldexplanation,submenu_header,total_sub_cost;
    LinearLayout submenu_header_layout;
    EditText value;

    public ChildWithSubMenu(View itemView) {
        super(itemView);
        fieldname=(TextView)itemView.findViewById(R.id.fieldname);
        submenu_header=(TextView)itemView.findViewById(R.id.submenu_header);
        total_sub_cost=(TextView)itemView.findViewById(R.id.total_sub_cost);
        fieldexplanation=(TextView)itemView.findViewById(R.id.fieldexplanation);
        submenu_header_layout=(LinearLayout) itemView.findViewById(R.id.submenu_header_layout);
        value=(EditText)itemView.findViewById(R.id.value);
    }
}
