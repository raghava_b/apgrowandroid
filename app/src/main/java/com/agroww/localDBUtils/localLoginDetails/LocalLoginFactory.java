package com.agroww.localDBUtils.localLoginDetails;

import android.util.Log;

import com.agroww.Responses.LoginResponse.LoginData;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */
public class LocalLoginFactory implements Iloginmethods {
    private static final String TAG = "LocalLoginFactory";

    private static LocalLoginFactory ourInstance = new LocalLoginFactory();

    public static LocalLoginFactory getInstance() {
        return ourInstance;
    }

    private LocalLoginFactory() {
    }

    @Override
    public void insert(Realm realm, final LoginData _loginData) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                Log.e(TAG, "execute: mobile"+_loginData.getMobile() );
                LocalLoginData loginData = realm.createObject(LocalLoginData.class);
                loginData.setUserId(_loginData.getUserId());
                loginData.setAadharId(_loginData.getAadharId());
                loginData.setAadharImageUrl(_loginData.getAadharImageUrl());
                loginData.setAddress(_loginData.getAddress());
                loginData.setCountry(_loginData.getCountry());
                loginData.setDistrict(_loginData.getDistrict());
                loginData.setDlImageUrl(_loginData.getDlImageUrl());
                loginData.setDob(_loginData.getDob());
                loginData.setFirstName(_loginData.getFirstName());
                loginData.setMobile(_loginData.getMobile());
                loginData.setMandal(_loginData.getMandal());
                loginData.setPanImageUrl(_loginData.getPanImageUrl());
                loginData.setState(_loginData.getState());
                loginData.setTaluk(_loginData.getTaluk());
                loginData.setVillage(_loginData.getVillage());
                Log.e(TAG, "execute: insertion" );
            }
        });
    }

    @Override
    public RealmResults<LocalLoginData> getLoginData(Realm realm, String userid) {
        RealmResults<LocalLoginData> loginDataRealmQuery = realm.where(LocalLoginData.class).equalTo("userId", userid).findAll();
        return loginDataRealmQuery;
    }
}
