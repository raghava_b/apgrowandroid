package com.agroww.localDBUtils.localLoginDetails;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public interface Iloginmethods {

    void insert(Realm realm, com.agroww.Responses.LoginResponse.LoginData loginData);
    RealmResults<LocalLoginData> getLoginData(Realm realm, String userid);
}
