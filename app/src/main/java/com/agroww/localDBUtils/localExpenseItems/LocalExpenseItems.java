package com.agroww.localDBUtils.localExpenseItems;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class LocalExpenseItems extends RealmObject {
    private String expenseItemId;
    private String expenseItemName;
    private String expenseCategoryId;
    private String expenseCategoryname;
    private String description;
    private int iteration;
    private String submenutext;
    private String sampleData;
    private int current_iteration;
    private String sumbmenuvalue;

    public int getCurrent_iteration() {
        return current_iteration;
    }

    public void setCurrent_iteration(int current_iteration) {
        this.current_iteration = current_iteration;
    }

    public String getSumbmenuvalue() {
        return sumbmenuvalue;
    }

    public void setSumbmenuvalue(String sumbmenuvalue) {
        this.sumbmenuvalue = sumbmenuvalue;
    }

    public String getSampleData() {
        return sampleData;
    }

    public void setSampleData(String sampleData) {
        this.sampleData = sampleData;
    }

    public int getIteration() {
        return iteration;
    }

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    public String getSubmenutext() {
        return submenutext;
    }

    public void setSubmenutext(String submenutext) {
        this.submenutext = submenutext;
    }

    public String getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(String expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    public String getExpenseItemName() {
        return expenseItemName;
    }

    public void setExpenseItemName(String expenseItemName) {
        this.expenseItemName = expenseItemName;
    }

    public String getExpenseCategoryId() {
        return expenseCategoryId;
    }

    public void setExpenseCategoryId(String expenseCategoryId) {
        this.expenseCategoryId = expenseCategoryId;
    }

    public String getExpenseCategoryname() {
        return expenseCategoryname;
    }

    public void setExpenseCategoryname(String expenseCategoryname) {
        this.expenseCategoryname = expenseCategoryname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
