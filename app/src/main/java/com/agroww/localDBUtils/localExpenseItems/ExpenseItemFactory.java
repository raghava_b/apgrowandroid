package com.agroww.localDBUtils.localExpenseItems;

import android.util.Log;

import com.agroww.Responses.ExpenseItems.ExpenseItem;
import com.agroww.dtos.NewHeaderItem;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */
public class ExpenseItemFactory implements IExpenseItemFunctions {
    private static ExpenseItemFactory ourInstance = new ExpenseItemFactory();
    public NewHeaderItem newHeaderItem;
    public List<LocalExpenseItems> childItems;
    LocalExpenseItems localExpenseItem;
    List<NewHeaderItem> listData;
    private static final String TAG = "ExpenseItemFactory";

    public static ExpenseItemFactory getInstance() {
        return ourInstance;
    }

    private ExpenseItemFactory() {
    }

    @Override
    public void insert(Realm realm, final ExpenseItem expenseItem) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                LocalExpenseItems localExpenseItems = realm.createObject(LocalExpenseItems.class);
                localExpenseItems.setExpenseCategoryId(expenseItem.getExpenseCategoryId());
                localExpenseItems.setExpenseItemId(expenseItem.getExpenseItemId());
                localExpenseItems.setDescription(expenseItem.getDescription());
                localExpenseItems.setExpenseItemName(expenseItem.getExpenseItemName());
                localExpenseItems.setExpenseCategoryname(expenseItem.getExpenseCategoryname());
                localExpenseItems.setSampleData("0");
                String id = expenseItem.getExpenseCategoryId();
                switch (id) {
                    case "1":
                        localExpenseItems.setIteration(0);
                        localExpenseItems.setSubmenutext("none");
                        break;
                    case "2":
                        localExpenseItems.setIteration(0);
                        localExpenseItems.setSubmenutext("none");
                        break;
                    case "3":
                        localExpenseItems.setIteration(3);
                        localExpenseItems.setSubmenutext("Fertilizer Dose");
                        break;
                    case "4":
                        localExpenseItems.setIteration(3);
                        localExpenseItems.setSubmenutext("Pesticides Dose");
                        break;
                    case "5":
                        localExpenseItems.setIteration(3);
                        localExpenseItems.setSubmenutext("Weeding/Weedicide Spray");
                        break;
                    case "6":
                        localExpenseItems.setIteration(3);
                        localExpenseItems.setSubmenutext("Irrigation");
                        break;
                    case "7":
                        localExpenseItems.setIteration(3);
                        localExpenseItems.setSubmenutext("Picking");
                        break;
                    case "8":
                        localExpenseItems.setIteration(3);
                        localExpenseItems.setSubmenutext("Storage Event");
                        break;
                    case "9":
                        localExpenseItems.setIteration(3);
                        localExpenseItems.setSubmenutext("Batch Processing");
                        break;
                    case "10":
                        localExpenseItems.setIteration(3);
                        localExpenseItems.setSubmenutext("Sale");
                        break;
                    case "11":
                        localExpenseItems.setIteration(0);
                        localExpenseItems.setSubmenutext("none");
                        break;
                    default:
                        break;

                }

            }
        });
    }

    @Override
    public RealmResults<LocalExpenseItems> getExpenseItems(Realm realm, String expenseItemid) {
        RealmResults<LocalExpenseItems> localExpenseItemses = realm.where(LocalExpenseItems.class).equalTo("expenseCategoryId", expenseItemid).findAll();
        Log.e(TAG, "getExpenseItems: iteration" + localExpenseItemses.get(0).getIteration());
        return localExpenseItemses;
    }

    @Override
    public RealmResults<LocalExpenseItems> getTotalExpenses(Realm realm) {
        RealmResults<LocalExpenseItems> localExpenseItemses = realm.where(LocalExpenseItems.class).findAll();
        return localExpenseItemses;
    }

    @Override
    public List<NewHeaderItem> getExpandableData(Realm realm) {

        listData = new ArrayList<>();
        // Land Preparation....
        RealmResults<LocalExpenseItems> landPeparationitems = getExpenseItems(realm, "1");
        addChildsforNotSubmenu(landPeparationitems);

        // Seeding/Sowing.....
        RealmResults<LocalExpenseItems> seedSowingitems = getExpenseItems(realm, "2");
        addChildsforNotSubmenu(seedSowingitems);

        RealmResults<LocalExpenseItems> fertilizerItems = getExpenseItems(realm, "3");
        addChilds(fertilizerItems);
        RealmResults<LocalExpenseItems> pesticidesItems = getExpenseItems(realm, "4");
        addChilds(pesticidesItems);
        RealmResults<LocalExpenseItems> weedingItems = getExpenseItems(realm, "5");
        addChilds(weedingItems);
        RealmResults<LocalExpenseItems> irrigationItems = getExpenseItems(realm, "6");
        addChilds(irrigationItems);
        RealmResults<LocalExpenseItems> pickingItems = getExpenseItems(realm, "7");
        addChilds(pickingItems);
        RealmResults<LocalExpenseItems> storageItems = getExpenseItems(realm, "8");
        addChilds(storageItems);
        RealmResults<LocalExpenseItems> batchItems = getExpenseItems(realm, "9");
        addChilds(batchItems);
        RealmResults<LocalExpenseItems> saleItems = getExpenseItems(realm, "10");
        addChilds(saleItems);
        RealmResults<LocalExpenseItems> otherItems = getExpenseItems(realm, "11");
        addChildsforNotSubmenu(otherItems);
        addTotalExpenses();
        addTotalEarned();
        return listData;
    }

    private void addTotalExpenses() {
        newHeaderItem = new NewHeaderItem();
        newHeaderItem.setHeadername("Total Expenses");
        newHeaderItem.setTotalvalue(0);
        newHeaderItem.setHeader_categoryid("12");
        childItems = new ArrayList<>();
        newHeaderItem.setChildItems(childItems);
        listData.add(newHeaderItem);
    }

    private void addTotalEarned() {
        newHeaderItem = new NewHeaderItem();
        newHeaderItem.setHeadername("Total Earned");
        newHeaderItem.setTotalvalue(0);
        newHeaderItem.setHeader_categoryid("13");
        childItems = new ArrayList<>();
        newHeaderItem.setChildItems(childItems);
        listData.add(newHeaderItem);
    }


    private void addChildsforNotSubmenu(RealmResults<LocalExpenseItems> items) {
        newHeaderItem = new NewHeaderItem();
        newHeaderItem.setHeadername(items.get(0).getExpenseCategoryname());
        newHeaderItem.setTotalvalue(0);
        newHeaderItem.setHeader_categoryid(items.get(0).getExpenseCategoryId());
        childItems = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            localExpenseItem = new LocalExpenseItems();
            localExpenseItem.setExpenseCategoryId(items.get(i).getExpenseCategoryId());
            localExpenseItem.setExpenseItemId(items.get(i).getExpenseItemId());
            localExpenseItem.setDescription(items.get(i).getDescription());
            localExpenseItem.setExpenseItemName(items.get(i).getExpenseItemName());
            localExpenseItem.setExpenseCategoryname(items.get(i).getExpenseCategoryname());
            localExpenseItem.setIteration(items.get(i).getIteration());
            localExpenseItem.setSampleData(items.get(i).getSampleData());
            localExpenseItem.setSubmenutext("none");
            localExpenseItem.setCurrent_iteration(0);
            childItems.add(localExpenseItem);
        }
        newHeaderItem.setChildItems(childItems);
        listData.add(newHeaderItem);
    }

    private void addChilds(RealmResults<LocalExpenseItems> items) {
        newHeaderItem = new NewHeaderItem();
        newHeaderItem.setHeadername(items.get(0).getExpenseCategoryname());
        newHeaderItem.setTotalvalue(0);
        newHeaderItem.setHeader_categoryid(items.get(0).getExpenseCategoryId());
        childItems = new ArrayList<>();
        int iteration = items.get(0).getIteration();
        Log.e(TAG, "getExpandableData: iteration" + iteration);
        int count = -1;
        int submenu_value = 0;
        int current_iteration = 1;
        for (int i = 0; i < (items.size() * iteration); i++) {

            if (i == 0) {
                count++;
            } else {
                if (i % (items.size()) == 0) {
                    current_iteration++;
                    count = 0;
                } else {
                    count++;
                }
            }

            localExpenseItem = new LocalExpenseItems();
            localExpenseItem.setExpenseCategoryId(items.get(count).getExpenseCategoryId());
            localExpenseItem.setExpenseItemId(items.get(count).getExpenseItemId());
            localExpenseItem.setDescription(items.get(count).getDescription());
            localExpenseItem.setExpenseItemName(items.get(count).getExpenseItemName());
            localExpenseItem.setExpenseCategoryname(items.get(count).getExpenseCategoryname());
            localExpenseItem.setIteration(items.get(count).getIteration());
            localExpenseItem.setSampleData(items.get(count).getSampleData());
            localExpenseItem.setCurrent_iteration(current_iteration);
            localExpenseItem.setSubmenutext("" + current_iteration + " " + items.get(count).getSubmenutext());
            submenu_value = submenu_value + Integer.parseInt(items.get(count).getSampleData());
            localExpenseItem.setSumbmenuvalue("" + submenu_value);
            childItems.add(localExpenseItem);
        }
        Log.e(TAG, "addChilds: childitemsize" + childItems.size());
        newHeaderItem.setChildItems(childItems);
        listData.add(newHeaderItem);
    }
}
