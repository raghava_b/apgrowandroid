package com.agroww.localDBUtils.localExpenseItems;

import com.agroww.Responses.ExpenseItems.ExpenseItem;
import com.agroww.dtos.HeaderItem;
import com.agroww.dtos.NewHeaderItem;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public interface IExpenseItemFunctions {

    void insert(Realm realm, ExpenseItem crops);
    RealmResults<LocalExpenseItems> getExpenseItems(Realm realm, String expenseItemid);
    RealmResults<LocalExpenseItems> getTotalExpenses(Realm realm);
    List<NewHeaderItem> getExpandableData(Realm realm);
}
