package com.agroww.localDBUtils.localCrops;

import com.agroww.Responses.CropVarities.Crops;
import com.agroww.Responses.Methods.Methods;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public interface ICropFunctions {

    void insert(Realm realm, Crops crops);
    RealmResults<LocalCrops> getCrops(Realm realm, String cropname);
    List<String> getCropNames(Realm realm);
}
