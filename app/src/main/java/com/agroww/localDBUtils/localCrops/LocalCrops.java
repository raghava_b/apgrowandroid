package com.agroww.localDBUtils.localCrops;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class LocalCrops extends RealmObject {
    private String cropVarietyId;
    private String cropVarietyName;
    private String description;
    private String cropFamily;
    private String timestamp;
    private String isActive;

    public String getCropVarietyId() {
        return cropVarietyId;
    }

    public void setCropVarietyId(String cropVarietyId) {
        this.cropVarietyId = cropVarietyId;
    }

    public String getCropVarietyName() {
        return cropVarietyName;
    }

    public void setCropVarietyName(String cropVarietyName) {
        this.cropVarietyName = cropVarietyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCropFamily() {
        return cropFamily;
    }

    public void setCropFamily(String cropFamily) {
        this.cropFamily = cropFamily;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

}
