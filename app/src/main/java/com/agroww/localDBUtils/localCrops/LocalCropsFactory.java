package com.agroww.localDBUtils.localCrops;

import com.agroww.Responses.CropVarities.Crops;
import com.agroww.Responses.Methods.Methods;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */
public class LocalCropsFactory implements ICropFunctions {
    private static LocalCropsFactory ourInstance = new LocalCropsFactory();

    public static LocalCropsFactory getInstance() {
        return ourInstance;
    }

    private LocalCropsFactory() {
    }

    @Override
    public void insert(Realm realm, final Crops crops) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                LocalCrops localCrops = realm.createObject(LocalCrops.class);
                localCrops.setCropVarietyId(crops.getCropVarietyId());
                localCrops.setCropVarietyName(crops.getCropVarietyName());
                localCrops.setIsActive(crops.getIsActive());
            }
        });
    }

    @Override
    public RealmResults<LocalCrops> getCrops(Realm realm, String cropname) {
        RealmResults<LocalCrops> results = realm.where(LocalCrops.class).equalTo("cropVarietyName", cropname).findAll();
        return results;
    }

    @Override
    public List<String> getCropNames(Realm realm) {
        RealmResults<LocalCrops> results = realm.where(LocalCrops.class).findAll();
        List<String> crops = new ArrayList<>();
        for (LocalCrops localCrops : results) {
            crops.add(localCrops.getCropVarietyName());
        }
        return crops;
    }
}
