package com.agroww.localDBUtils.localSeasons;

import com.agroww.Responses.Seasons.Seasons;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */
public class LocalSeasonsFactory implements ISeasonMethods {
    private static LocalSeasonsFactory ourInstance = new LocalSeasonsFactory();

    public static LocalSeasonsFactory getInstance() {
        return ourInstance;
    }

    private LocalSeasonsFactory() {
    }


    @Override
    public void insert(Realm realm, final Seasons seasons) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                LocalSeasons localSeasons = realm.createObject(LocalSeasons.class);
                localSeasons.setSeasonId(seasons.getSeasonId());
                localSeasons.setSeasonName(seasons.getSeasonName());
                localSeasons.setDescription(seasons.getDescription());
            }
        });
    }

    @Override
    public RealmResults<LocalSeasons> getSeasons(Realm realm, String seasonname) {
        RealmResults<LocalSeasons> results = realm.where(LocalSeasons.class).equalTo("seasonName", seasonname).findAll();
        return results;
    }

    @Override
    public List<String> getSeaonNames(Realm realm) {
        RealmResults<LocalSeasons> results = realm.where(LocalSeasons.class).findAll();
        List<String> seasons = new ArrayList<>();
        for (LocalSeasons localSeasons : results) {
            seasons.add(localSeasons.getSeasonName());
        }
        return seasons;
    }
}
