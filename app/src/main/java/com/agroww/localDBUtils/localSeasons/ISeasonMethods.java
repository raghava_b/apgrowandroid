package com.agroww.localDBUtils.localSeasons;

import com.agroww.Responses.Seasons.Seasons;

import org.json.JSONObject;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public interface ISeasonMethods {

    void insert(Realm realm, Seasons seasons);
    RealmResults<LocalSeasons> getSeasons(Realm realm,String seasonid);
    List<String> getSeaonNames(Realm realm);
}
