package com.agroww.localDBUtils.localMethods;

import com.agroww.Responses.Methods.Methods;
import com.agroww.Responses.Seasons.Seasons;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public interface IMethods {

    void insert(Realm realm, Methods seasons);
    RealmResults<LocalMethods> getMethods(Realm realm, String methodname);
    List<String> getMethodNames(Realm realm);
}
