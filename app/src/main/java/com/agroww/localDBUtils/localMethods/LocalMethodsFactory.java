package com.agroww.localDBUtils.localMethods;

import com.agroww.Responses.Methods.Methods;
import com.agroww.Responses.Seasons.Seasons;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */
public class LocalMethodsFactory implements IMethods {
    private static LocalMethodsFactory ourInstance = new LocalMethodsFactory();

    public static LocalMethodsFactory getInstance() {
        return ourInstance;
    }

    private LocalMethodsFactory() {
    }

    @Override
    public void insert(Realm realm, final Methods methods) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                LocalMethods localMethods = realm.createObject(LocalMethods.class);
                localMethods.setMethodId(methods.getMethodId());
                localMethods.setMethodName(methods.getMethodName());
                localMethods.setDescription(methods.getDescription());
            }
        });
    }

    @Override
    public RealmResults<LocalMethods> getMethods(Realm realm, String methodname) {
        RealmResults<LocalMethods> results = realm.where(LocalMethods.class).equalTo("methodName", methodname).findAll();
        return results;
    }

    @Override
    public List<String> getMethodNames(Realm realm) {
        RealmResults<LocalMethods> results = realm.where(LocalMethods.class).findAll();
        List<String> methods = new ArrayList<>();
        for (LocalMethods localMethods : results) {
            methods.add(localMethods.getMethodName());
        }
        return methods;
    }

}
