package com.agroww;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by raghavakumarburugadda on 28/05/17.
 */

public class UserProfileFragment extends Fragment {

    View root_view;

    @BindView(R.id.farming_image)
    CircleImageView farming_image;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.mobilenumber)
    TextView mobilenumber;
    @BindView(R.id.changepassword)
    Button changepassword;
    @BindView(R.id.changeuserinfo)
    Button changeuserinfo;
    @BindView(R.id.btChooseFile)
    Button btChooseFile;
    @BindView(R.id.btLogout)
    Button btLogout;
    private int SELECT_SINGLE_PICTURE = 101;

    public static UserProfileFragment newInstance(Bundle bundle) {
        UserProfileFragment userProfileFragment = new UserProfileFragment();
        userProfileFragment.setArguments(bundle);
        return userProfileFragment;
    }

    @OnClick(R.id.changeuserinfo)
    public void gotoUserInfo()
    {
        Intent intent=new Intent(getActivity(),ChangeUserInfoFragment.class);
        startActivity(intent);
    }
    @OnClick(R.id.changepassword)
    public void gotoChangePassword()
    {
        FragmentTransaction fragmentTransaction=getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.profile_container,ChangePasswordFragment.newInstance(null));
        fragmentTransaction.commit();
    }
    @OnClick(R.id.btChooseFile)
    public void goToGallery()
    {
        Intent intent = new Intent();
// Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_SINGLE_PICTURE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        root_view = inflater.inflate(R.layout.fragment_user_profile, null);
        ButterKnife.bind(this, root_view);
        return root_view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == SELECT_SINGLE_PICTURE && resultCode==RESULT_OK) {

                try {
                    Uri selectedImageUri = data.getData();
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageUri);
                    farming_image.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
