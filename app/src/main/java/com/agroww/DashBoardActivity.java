package com.agroww;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashBoardActivity extends BaseActivity {

    @BindView(R.id.farming_layout)
    LinearLayout farming_layout;
    @BindView(R.id.fisheries_layout)
    LinearLayout fisheries_layout;
    @BindView(R.id.dairy_layout)
    LinearLayout dairy_layout;
    @BindView(R.id.poultry_layout)
    LinearLayout poultry_layout;
    @BindView(R.id.userprofile_layout)
    LinearLayout userprofile_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setupToolbar(false,false,true);
    }
    @OnClick(R.id.farming_layout)
    public void onFarmingClicked()
    {
        Intent intent=new Intent(DashBoardActivity.this,AddFarmingActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.fisheries_layout)
    public void onFisherisClicked()
    {

    }
    @OnClick(R.id.profile_layout)
    public void onProfileisClicked()
    {
        Intent intent=new Intent(DashBoardActivity.this,UserProfileActivity.class);
        startActivity(intent);
    }
    @Override
    public int getLayoutResource() {
        return R.layout.activity_dash_board;
    }

    @Override
    public String getToolbarName() {
        return "AgrowBook";
    }

    @Override
    public void onBackPressed() {

    }
}
