package com.agroww;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserProfileActivity extends BaseActivity {

    @BindView(R.id.profile_container)
    FrameLayout profile_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.profile_container,UserProfileFragment.newInstance(null));
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_user_profile;
    }

    @Override
    public String getToolbarName() {
        return "UserProfile";
    }
}
