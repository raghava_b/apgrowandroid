package com.agroww;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agroww.dtos.ChildItem;
import com.agroww.dtos.HeaderItem;
import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;

import java.util.List;

import apcare.apgrowcomponents.FarmingPopupView;
import apcare.apgrowcomponents.IUpdateClick;

/**
 * Created by raghavakumarburugadda on 28/05/17.
 */

public class FarmingAdapter extends ExpandableRecyclerAdapter<HeaderItem, ChildItem, HeaderViewHodler, ChildViewHolder> {
    /**
     * Primary constructor. Sets up {@link #mParentList} and {@link #mFlatItemList}.
     * <p>
     * Any changes to {@link #mParentList} should be made on the original instance, and notified via
     * {@link #notifyParentInserted(int)}
     * {@link #notifyParentRemoved(int)}
     * {@link #notifyParentChanged(int)}
     * {@link #notifyParentRangeInserted(int, int)}
     * {@link #notifyChildInserted(int, int)}
     * {@link #notifyChildRemoved(int, int)}
     * {@link #notifyChildChanged(int, int)}
     * methods and not the notify methods of RecyclerView.Adapter.
     *
     * @param parentList List of all parents to be displayed in the RecyclerView that this
     * adapter is linked to
     */
    private LayoutInflater mInflater;
    private List<HeaderItem> mheaderItems;
    public static final int CHILD_WITHOUTSUBMENU = 1;
    public static final int CHILD_WITHSUBMENU = 2;
    public Context mContext;
    private static final String TAG = "FarmingAdapter";
    private View rootview;
    private static int child_submenu_cost = 0;

    public FarmingAdapter(Context context, @NonNull List<HeaderItem> parentList, View root_view) {
        super(parentList);
        mheaderItems = parentList;
        mInflater = LayoutInflater.from(context);
        mContext = context;
        rootview = root_view;
    }

    @NonNull
    @Override
    public HeaderViewHodler onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View headerview = mInflater.inflate(R.layout.headerlayout, parentViewGroup, false);
        return new HeaderViewHodler(headerview);
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View childView;
        if (viewType == CHILD_WITHSUBMENU) {
            childView = mInflater.inflate(R.layout.childlayout_withsubmenu, childViewGroup, false);
            ChildWithSubMenu childWithSubMenu = new ChildWithSubMenu(childView);
            return childWithSubMenu;
        } else if (viewType == CHILD_WITHOUTSUBMENU) {
            childView = mInflater.inflate(R.layout.childlayout_withoutsubmenu, childViewGroup, false);
            ChildWithoutSubmenu childWithoutSubmenu = new ChildWithoutSubmenu(childView);
            return childWithoutSubmenu;
        } else {
            childView = mInflater.inflate(R.layout.childlayout_withsubmenu, childViewGroup, false);
            ChildWithoutSubmenu childWithoutSubmenu = new ChildWithoutSubmenu(childView);
            return childWithoutSubmenu;
        }
    }

    @Override
    public void onBindParentViewHolder(@NonNull HeaderViewHodler parentViewHolder, int parentPosition, @NonNull HeaderItem parent) {
        parentViewHolder.header_text.setText(parent.getHeadername());

        if (parent.getHeadername().equalsIgnoreCase("Total Expenses") || parent.getHeadername().equalsIgnoreCase("Total Earned")) {
            parentViewHolder.header_layout.setBackgroundResource(R.drawable.rectanglebox);
            parentViewHolder.header_text.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            parentViewHolder.total_cost.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        } else {
            parentViewHolder.header_layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            parentViewHolder.header_text.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            parentViewHolder.total_cost.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));

        }


        List<ChildItem> childItems = parent.getChildList();
        int sum = 0;
        for (int i = 0; i < childItems.size(); i++) {
            if (childItems.get(i).getExampledata().equalsIgnoreCase("")) {
                sum = sum + 0;
            } else {
                sum = sum + Integer.parseInt(childItems.get(i).getExampledata());
            }

        }
        parentViewHolder.total_cost.setText("" + sum);
    }

    @Override
    public void onBindChildViewHolder(@NonNull ChildViewHolder childViewHolder, final int parentPosition, final int childPosition, @NonNull ChildItem child) {
        if (childViewHolder instanceof ChildWithSubMenu) {
            ChildWithSubMenu childWithSubMenu = (ChildWithSubMenu) childViewHolder;
            childWithSubMenu.submenu_header.setText(child.getSubmenu());
            childWithSubMenu.fieldname.setText(child.getFieldame());
            childWithSubMenu.fieldexplanation.setText(child.getExplanation());
            childWithSubMenu.value.setTag(childPosition);
            childWithSubMenu.value.setText(child.getExampledata());

            if (!child.getExampledata().equalsIgnoreCase(""))
                child_submenu_cost = child_submenu_cost + Integer.parseInt(child.getExampledata());

            if (!mheaderItems.get(parentPosition).getHeadername().equalsIgnoreCase("Processing") && !mheaderItems.get(parentPosition).getHeadername().equalsIgnoreCase("Selling")) {
                if (childPosition % 3 == 0) {
                    childWithSubMenu.submenu_header_layout.setVisibility(View.VISIBLE);
                    childWithSubMenu.total_sub_cost.setText("" + child_submenu_cost);
                    child_submenu_cost = 0;
                } else {
                    childWithSubMenu.submenu_header_layout.setVisibility(View.GONE);
                }
            } else if (mheaderItems.get(parentPosition).getHeadername().equalsIgnoreCase("Processing")) {
                if (childPosition % 4 == 0) {
                    childWithSubMenu.submenu_header_layout.setVisibility(View.VISIBLE);
                    childWithSubMenu.total_sub_cost.setText("" + child_submenu_cost);
                    child_submenu_cost = 0;
                } else {
                    childWithSubMenu.submenu_header_layout.setVisibility(View.GONE);
                }
            } else {
                if (childPosition % 6 == 0) {
                    childWithSubMenu.submenu_header_layout.setVisibility(View.VISIBLE);
                    childWithSubMenu.total_sub_cost.setText("" + child_submenu_cost);
                    child_submenu_cost = 0;
                } else {
                    childWithSubMenu.submenu_header_layout.setVisibility(View.GONE);
                }
            }


            childWithSubMenu.value.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                @Override
                public void onClick(View view) {

                    Log.e(TAG, "onClick: dkfjdks");

                    IUpdateClick iUpdateClick = new IUpdateClick() {
                        @Override
                        public void getDateandValue(String date, String value) {
                            Log.e(TAG, "getDateandValue: date:" + date + "value:" + value);
                            mheaderItems.get(parentPosition).getChildList().get(childPosition).setExampledata(value);
                            notifyChildChanged(parentPosition, childPosition);
                            notifyDataSetChanged();
                        }
                    };
                    FarmingPopupView farmingPopupView = new FarmingPopupView(mContext, "Title", iUpdateClick, rootview);
                    farmingPopupView.initView();
                }
            });
        } else {
            ChildWithoutSubmenu childWithSubMenu = (ChildWithoutSubmenu) childViewHolder;
            childWithSubMenu.fieldname.setText(child.getFieldame());
            childWithSubMenu.fieldexplanation.setText(child.getExplanation());
            childWithSubMenu.value.setTag(childPosition);
            childWithSubMenu.value.setText(child.getExampledata());
            if (child.getFieldame().equalsIgnoreCase("")) child_submenu_cost = child_submenu_cost + Integer.parseInt(child.getExampledata());

            childWithSubMenu.value.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                @Override
                public void onClick(View view) {

                    Log.e(TAG, "onClick: child with submenu" );
                    IUpdateClick iUpdateClick = new IUpdateClick() {
                        @Override
                        public void getDateandValue(String date, String value) {
                            Log.e(TAG, "getDateandValue: date:" + date + "value:" + value);
                            mheaderItems.get(parentPosition).getChildList().get(childPosition).setExampledata(value);
                            notifyChildChanged(parentPosition, childPosition);
                            notifyParentChanged(parentPosition);
                        }
                    };
                    FarmingPopupView farmingPopupView = new FarmingPopupView(mContext, "Title", iUpdateClick, rootview);
                    farmingPopupView.initView();
                }
            });
        }
    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {
        ChildItem childItem = mheaderItems.get(parentPosition).getChildList().get(childPosition);
        if (childItem.getSubmenu().equalsIgnoreCase("none")) {
            return CHILD_WITHOUTSUBMENU;
        } else {
            return CHILD_WITHSUBMENU;
        }
    }
}
