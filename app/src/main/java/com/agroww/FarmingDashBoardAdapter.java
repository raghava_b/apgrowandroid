package com.agroww;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agroww.dtos.FarmingData;

import java.util.List;

/**
 * Created by raghavakumarburugadda on 28/05/17.
 */

public class FarmingDashBoardAdapter extends RecyclerView.Adapter<FarmingDashBoardAdapter.FarmingDashboardViewHolder>{



    List<FarmingData> farmingData;
    Context mContext;
    LayoutInflater layoutInflater;

    public FarmingDashBoardAdapter(Context context, List<FarmingData> _farmingDatas)
    {
        mContext=context;
        farmingData=_farmingDatas;
        layoutInflater=LayoutInflater.from(mContext);
    }


    @Override
    public FarmingDashboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=layoutInflater.inflate(R.layout.farming_dashboard_layout,null);
        FarmingDashboardViewHolder farmingDashboardViewHolder=new FarmingDashboardViewHolder(view);

        return farmingDashboardViewHolder;
    }

    @Override
    public void onBindViewHolder(FarmingDashboardViewHolder holder, int position) {
        String text=farmingData.get(position).getCrop()+" "+farmingData.get(position).getSeason()+"  "+farmingData.get(position).getYear();
        holder.title_text.setText(text);
        holder.dashboard_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext,SampleActivity.class);
                mContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return farmingData.size();
    }

    class FarmingDashboardViewHolder extends RecyclerView.ViewHolder
    {
        TextView title_text;
        CardView dashboard_item;
        public FarmingDashboardViewHolder(View itemView) {
            super(itemView);
            title_text=(TextView)itemView.findViewById(R.id.title_text);
            dashboard_item=(CardView)itemView.findViewById(R.id.dashboard_item);
        }
    }
}
