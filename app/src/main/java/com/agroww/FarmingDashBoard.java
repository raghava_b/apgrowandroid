package com.agroww;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

public class FarmingDashBoard extends BaseActivity {

    @BindView(R.id.farming_list)
    RecyclerView farming_list;
    AgrowApp agrowApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        setupToolbar(true,true,true);
        left_arrow.setBackgroundResource(R.drawable.home);
        home.setBackgroundResource(R.drawable.add);
        agrowApp=AgrowApp.getInstance();

        FarmingDashBoardAdapter farmingDashBoardAdapter=new FarmingDashBoardAdapter(FarmingDashBoard.this,agrowApp.getFarmingDatas());
        farming_list.setLayoutManager(new LinearLayoutManager(FarmingDashBoard.this));
        farming_list.setAdapter(farmingDashBoardAdapter);

    }

    @OnClick(R.id.home)
    void onAddClicked()
    {
        Intent intent=new Intent(FarmingDashBoard.this,AddFarmingActivity.class);
        startActivity(intent);
    }

    @OnTouch(R.id.left_arrow)
    public boolean onBackarrowClicked() {
        Intent intent = new Intent(this, DashBoardActivity.class);
        startActivity(intent);
        return true;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_farming_dash_board;
    }

    @Override
    public String getToolbarName() {
        return "Farming";
    }
}
