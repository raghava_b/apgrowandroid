package com.agroww;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.agroww.Requests.LoginRequest;
import com.agroww.Responses.LoginResponse.LoginResponse;
import com.agroww.localDBUtils.localLoginDetails.LocalLoginData;
import com.agroww.localDBUtils.localLoginDetails.LocalLoginFactory;
import com.agroww.networkutils.VolleyCallback;
import com.agroww.networkutils.VolleyUtilityClass;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class LoginActivity extends BaseActivity implements Validator.ValidationListener {

    @Length(min = 10, max = 10)
    @NotEmpty
    @BindView(R.id.mobilenumber_edittext)
    EditText mobilenumber_edittext;
    @Password(min = 6, scheme = Password.Scheme.ALPHA)
    @NotEmpty
    @BindView(R.id.password_edittext)
    EditText password_edittext;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.register)
    Button register;
    Validator validator;
    private static final String TAG = "LoginActivity";
    private Realm realm;
    private Gson gson;
    static final Integer READ_EXST = 0x5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        setupToolbar(false, false, true);
        realm = Realm.getDefaultInstance();
        covertImgtoBase64("");
        askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE,READ_EXST);

    }

    public void covertImgtoBase64(String fileName)
    {
        try
        {
            //InputStream inputStream = new FileInputStream(fileName);//You can get an inputStream using any IO API

            Drawable drawable = getResources().getDrawable(R.drawable.user);
            BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
            Bitmap bitmap = bitmapDrawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream); //use the compression format of your need
            InputStream inputStream = new ByteArrayInputStream(stream.toByteArray());
            byte[] bytes;
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bytes = output.toByteArray();
            String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
           // System.out.println(encodedString);
            //Log.e(TAG, "covertImgtoBase64: "+encodedString );

            JSONObject jsonObject=new JSONObject();
            jsonObject.put("file",encodedString);
            System.out.println(jsonObject);
           // Log.e(TAG, "covertImgtoBase64: "+jsonObject );

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.login)
    public void onLoginClicked() {
        validator.validate();
    }

    @OnClick(R.id.register)
    public void onRegistrationClicked() {
        Intent intent = new Intent(LoginActivity.this, CreateAccountActivity.class);
        startActivity(intent);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    public String getToolbarName() {
        return "Login";
    }

    @Override
    public void onValidationSucceeded() {
        callLoginAPI();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void callLoginAPI() {
        VolleyCallback volleyCallback = new VolleyCallback() {
            @Override
            public void volleyResponse(String string) {
                Log.e(TAG, "volleyResponse: " + string);
                if (!checkIsNullorNot(string)) {
                    parseandSaveLoginAPI(string);
                } else {
                    showAlertDialog(getString(R.string.alert), getString(R.string.networkerror));
                }

            }
        };
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("phone", mobilenumber_edittext.getText().toString());
            jsonObject.put("password", password_edittext.getText().toString());
            String url = getString(R.string.base_url) + "loginUser";
            VolleyUtilityClass volleyUtilityClass = VolleyUtilityClass.getInstance(LoginActivity.this);
            volleyUtilityClass.callPOSTAPI(url, jsonObject, volleyCallback);
        } catch (Exception e) {

        }
    }

    private void parseandSaveLoginAPI(String response) {

        try {
            AgrowApp.agrowApp.loginResponse = new Gson().fromJson(response, LoginResponse.class);
            Log.e(TAG, "parseandSaveLoginAPI: " +  AgrowApp.agrowApp.loginResponse.getData().size());
            LocalLoginFactory localLoginFactory = LocalLoginFactory.getInstance();
            Log.e(TAG, "data: mobile" +  AgrowApp.agrowApp.loginResponse.getData().get(0).getMobile());
            localLoginFactory.insert(realm,  AgrowApp.agrowApp.loginResponse.getData().get(0));

            RealmResults<LocalLoginData> loginDataRealmQuery = localLoginFactory.getLoginData(realm,  AgrowApp.agrowApp.loginResponse.getData().get(0).getUserId());
            Log.e(TAG, "parseandSaveLoginAPI:Mobile " + loginDataRealmQuery.size());

            Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(LoginActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED){
            switch (requestCode) {
                //Read External Storage
               /* case 4:
                    Intent imageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(imageIntent, 11);
                    break;*/

            }

            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            validator.setValidationListener(this);
            askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE,READ_EXST);
        }
    }
}
