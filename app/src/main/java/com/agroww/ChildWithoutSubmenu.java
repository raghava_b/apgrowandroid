package com.agroww;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;

/**
 * Created by raghavakumarburugadda on 27/05/17.
 */

public class ChildWithoutSubmenu extends ChildViewHolder{
    TextView fieldname,fieldexplanation;
    EditText value;

    public ChildWithoutSubmenu(View itemView) {
        super(itemView);
        fieldname=(TextView)itemView.findViewById(R.id.fieldname);
        fieldexplanation=(TextView)itemView.findViewById(R.id.fieldexplanation);
        value=(EditText)itemView.findViewById(R.id.value);
    }
}
