package com.agroww.Responses.ExpenseItems;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class ExpenseItem{
    @SerializedName("expense_item_id")
    @Expose
    private String expenseItemId;
    @SerializedName("expense_item_name")
    @Expose
    private String expenseItemName;
    @SerializedName("expense_category_id")
    @Expose
    private String expenseCategoryId;
    @SerializedName("expense_categoryname")
    @Expose
    private String expenseCategoryname;
    @SerializedName("description")
    @Expose
    private String description;

    public String getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(String expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    public String getExpenseItemName() {
        return expenseItemName;
    }

    public void setExpenseItemName(String expenseItemName) {
        this.expenseItemName = expenseItemName;
    }

    public String getExpenseCategoryId() {
        return expenseCategoryId;
    }

    public void setExpenseCategoryId(String expenseCategoryId) {
        this.expenseCategoryId = expenseCategoryId;
    }

    public String getExpenseCategoryname() {
        return expenseCategoryname;
    }

    public void setExpenseCategoryname(String expenseCategoryname) {
        this.expenseCategoryname = expenseCategoryname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
