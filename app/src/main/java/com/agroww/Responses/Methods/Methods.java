package com.agroww.Responses.Methods;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class Methods{
    @SerializedName("method_id")
    @Expose
    private String methodId;
    @SerializedName("method_name")
    @Expose
    private String methodName;
    @SerializedName("description")
    @Expose
    private String description;

    public String getMethodId() {
        return methodId;
    }

    public void setMethodId(String methodId) {
        this.methodId = methodId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
