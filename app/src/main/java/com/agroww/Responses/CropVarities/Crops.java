package com.agroww.Responses.CropVarities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class Crops {

    @SerializedName("crop_variety_id")
    @Expose
    private String cropVarietyId;
    @SerializedName("crop_variety_name")
    @Expose
    private String cropVarietyName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("crop_family")
    @Expose
    private String cropFamily;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("is_active")
    @Expose
    private String isActive;

    public String getCropVarietyId() {
        return cropVarietyId;
    }

    public void setCropVarietyId(String cropVarietyId) {
        this.cropVarietyId = cropVarietyId;
    }

    public String getCropVarietyName() {
        return cropVarietyName;
    }

    public void setCropVarietyName(String cropVarietyName) {
        this.cropVarietyName = cropVarietyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCropFamily() {
        return cropFamily;
    }

    public void setCropFamily(String cropFamily) {
        this.cropFamily = cropFamily;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

}
