package com.agroww.Responses.GetUserbalancesheets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by raghavakumarburugadda on 04/06/17.
 */

public class GetUserBalanceSheetResponse {
    @SerializedName("endPoint")
    @Expose
    private String endPoint;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<GetBalanceSheet> data = null;

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetBalanceSheet> getData() {
        return data;
    }

    public void setData(List<GetBalanceSheet> data) {
        this.data = data;
    }

}
