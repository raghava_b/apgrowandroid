package com.agroww.Responses.GetUserbalancesheets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by raghavakumarburugadda on 04/06/17.
 */

public class GetBalanceSheet {

    @SerializedName("crop_id")
    @Expose
    private String cropId;
    @SerializedName("method_id")
    @Expose
    private String methodId;
    @SerializedName("method_name")
    @Expose
    private String methodName;
    @SerializedName("season_id")
    @Expose
    private String seasonId;
    @SerializedName("season_name")
    @Expose
    private String seasonName;
    @SerializedName("crop_variety_id")
    @Expose
    private String cropVarietyId;
    @SerializedName("crop_variety_name")
    @Expose
    private String cropVarietyName;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("expense_cat_mst_id")
    @Expose
    private String expenseCatMstId;
    @SerializedName("expense_item_id")
    @Expose
    private String expenseItemId;
    @SerializedName("expense_item_name")
    @Expose
    private String expenseItemName;
    @SerializedName("expense_item_val")
    @Expose
    private String expenseItemVal;
    @SerializedName("iteration")
    @Expose
    private String iteration;
    @SerializedName("expense_date")
    @Expose
    private String expenseDate;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("invoice_url")
    @Expose
    private String invoiceUrl;
    @SerializedName("notes")
    @Expose
    private String notes;

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getMethodId() {
        return methodId;
    }

    public void setMethodId(String methodId) {
        this.methodId = methodId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public String getCropVarietyId() {
        return cropVarietyId;
    }

    public void setCropVarietyId(String cropVarietyId) {
        this.cropVarietyId = cropVarietyId;
    }

    public String getCropVarietyName() {
        return cropVarietyName;
    }

    public void setCropVarietyName(String cropVarietyName) {
        this.cropVarietyName = cropVarietyName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getExpenseCatMstId() {
        return expenseCatMstId;
    }

    public void setExpenseCatMstId(String expenseCatMstId) {
        this.expenseCatMstId = expenseCatMstId;
    }

    public String getExpenseItemId() {
        return expenseItemId;
    }

    public void setExpenseItemId(String expenseItemId) {
        this.expenseItemId = expenseItemId;
    }

    public String getExpenseItemName() {
        return expenseItemName;
    }

    public void setExpenseItemName(String expenseItemName) {
        this.expenseItemName = expenseItemName;
    }

    public String getExpenseItemVal() {
        return expenseItemVal;
    }

    public void setExpenseItemVal(String expenseItemVal) {
        this.expenseItemVal = expenseItemVal;
    }

    public String getIteration() {
        return iteration;
    }

    public void setIteration(String iteration) {
        this.iteration = iteration;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getInvoiceUrl() {
        return invoiceUrl;
    }

    public void setInvoiceUrl(String invoiceUrl) {
        this.invoiceUrl = invoiceUrl;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


}
