package com.agroww.Responses.Seasons;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class Seasons{
    @SerializedName("season_id")
    @Expose
    private String seasonId;
    @SerializedName("season_name")
    @Expose
    private String seasonName;
    @SerializedName("description")
    @Expose
    private String description;

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
