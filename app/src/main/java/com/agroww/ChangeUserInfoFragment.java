package com.agroww;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agroww.Responses.LoginResponse.LoginData;
import com.agroww.networkutils.VolleyCallback;
import com.agroww.networkutils.VolleyUtilityClass;
import com.google.gson.JsonObject;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import apcare.apgrowcomponents.ApgrowPopUpImageView;
import apcare.apgrowcomponents.ApgrowPopupView;
import apcare.apgrowcomponents.DatePickerUtils;
import apcare.apgrowcomponents.ICardClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.internal.Utils;

/**
 * Created by raghavakumarburugadda on 28/05/17.
 */

public class ChangeUserInfoFragment extends BaseActivity implements Validator.ValidationListener {

    private static final String TAG ="ChangeUserInfoFragment" ;
    private static final int IMAGE_CODE =221 ;
    private static final int UPDATE_CODE =222 ;
    private View root_view;
    Validator validator;
    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.firstname_edittext)
    EditText firstname_edittext;

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.lastname_edittext)
    EditText lastname_edittext;

    @NotEmpty
    @BindView(R.id.dob_edittext)
    EditText dob_edittext;

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.address_edittext)
    EditText address_edittext;

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.village_edittext)
    EditText village_edittext;

    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.mandal_edittext)
    EditText mandal_edittext;


    @Email
    @NotEmpty
    @BindView(R.id.email_edittext)
    EditText email_edittext;


    @Length(min = 2)
    @NotEmpty
    @BindView(R.id.district_edittext)
    EditText district_edittext;

    @BindView(R.id.create_account)
    Button create_account;
    @BindView(R.id.gender_layout)
    RelativeLayout gender_layout;
    @BindView(R.id.country_layout)
    RelativeLayout country_layout;
    @BindView(R.id.state_layout)
    RelativeLayout state_layout;


    @BindView(R.id.genderText)
    TextView genderText;
    @BindView(R.id.country_text)
    TextView country_text;
    @BindView(R.id.state_text)
    TextView state_text;

    @BindView(R.id.activity_changeuserinfo_layout)
    LinearLayout activity_changeuserinfo_layout;

    @BindView((R.id.btAdChooseFile))
    Button btAdChooseFile;
    @BindView((R.id.btPaChooseFile))
    Button btPaChooseFile;
    @BindView((R.id.btLiChooseFile))
    Button btLiChooseFile;
    @BindView((R.id.aadhar_img))
    ImageView aadhar_img;
    @BindView((R.id.pan_img))
    ImageView pan_img;
    @BindView((R.id.licence_img))
    ImageView licence_img;

    private int SELECT_AADAR_PICTURE = 101;
    private int SELECT_PAN_PICTURE = 102;
    private int SELECT_LICENCE_PICTURE = 103;

    LoginData loginData;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        loginData =  AgrowApp.agrowApp.loginResponse.getData().get(0);
        DatePickerUtils datePickerUtils = new DatePickerUtils(dob_edittext, this);
        initLoginData();
        setupToolbar(true,true,true);
    }

    private void initLoginData() {
        HashMap<String,String> genderMap = new HashMap<>();
        genderMap.put("0","Female");
        genderMap.put("1","Male");
        genderMap.put("0","Others");


        firstname_edittext.setText(loginData.getFirstName());
        lastname_edittext.setText(loginData.getLastName());
        dob_edittext.setText(loginData.getDob());
        email_edittext.setText(loginData.getEmail());
        mandal_edittext.setText(loginData.getMandal());
        address_edittext.setText(loginData.getAddress());
        district_edittext.setText(loginData.getDistrict());
        village_edittext.setText(loginData.getVillage());
        country_text.setText(loginData.getCountry());
        genderText.setText(genderMap.get(loginData.getGender()));
        state_text.setText(genderMap.get(loginData.getState()));


    }

    @OnClick(R.id.country_layout)
    public void onCountryClicked() {
        setupCountries();
    }

    @OnClick(R.id.gender_layout)
    public void onGenderClicked() {
        setupGender();
    }

    @OnClick(R.id.state_layout)
    public void onStateClicked() {
        setupStates();
    }

    @OnClick(R.id.create_account)
    public void createAccount() {
            validator.validate();

    }
    @OnClick(R.id.btAdChooseFile)
    public void addAdarImage() {
      openGallery(SELECT_AADAR_PICTURE);

    }

    private void openGallery(int SELECT_SINGLE_PICTURE) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_SINGLE_PICTURE);
    }

    @OnClick(R.id.btPaChooseFile)
    public void addPanImage() {
        openGallery(SELECT_PAN_PICTURE);
    }
    @OnClick(R.id.btLiChooseFile)
    public void addLiImage() {
        openGallery(SELECT_LICENCE_PICTURE);
    }

    private void setupCountries() {
        final ArrayList<String> countries = new ArrayList<>();
        countries.add("INDIA");
        countries.add("USA");
        countries.add("UK");
        countries.add("SPAIN");
        countries.add("ISTAMBUL");
        countries.add("TURKEY");

        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text,int position) {
                Log.e("", "clickedPosition: " + text);
                country_text.setText(text);
            }
        };

        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(ChangeUserInfoFragment.this, "Countries", countries, iCardClick, activity_changeuserinfo_layout);
        apgrowPopupView.initView();
    }

    @Override
    public void onBackPressed() {

    }

    private void setupGender() {
        final ArrayList<String> gender = new ArrayList<>();
        gender.add("Male");
        gender.add("Female");
        gender.add("Other");
        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text,int positon) {
                Log.e("", "clickedPosition: " + text);
                genderText.setText(text);
            }
        };

        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(ChangeUserInfoFragment.this, "Gender", gender, iCardClick, activity_changeuserinfo_layout);
        apgrowPopupView.initView();
    }

    private void setupStates() {
        final ArrayList<String> states = new ArrayList<>();
        states.add("Andhra Pradesh");
        states.add("Tamil Nadu");
        states.add("Telangana");
        states.add("Kerala");
        states.add("Karnataka");
        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text,int position) {
                Log.e("", "clickedPosition: " + text);
                state_text.setText(text);
            }
        };

        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(ChangeUserInfoFragment.this, "States", states, iCardClick, activity_changeuserinfo_layout);
        apgrowPopupView.initView();
    }


    @Override
    public int getLayoutResource() {
        return R.layout.fragment_change_userinfo;
    }

    @Override
    public String getToolbarName() {
        return "Change User Information";
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (resultCode==RESULT_OK) {

                try {
                    Uri selectedImageUri = data.getData();
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                    if(requestCode == SELECT_AADAR_PICTURE)
                    aadhar_img.setImageBitmap(bitmap);
                    else  if(requestCode == SELECT_PAN_PICTURE)
                        pan_img.setImageBitmap(bitmap);
                    else  if(requestCode == SELECT_LICENCE_PICTURE)
                        licence_img.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    @OnClick(R.id.aadhar_img)
    public void openAAImagePopUp()
    {
        if(((BitmapDrawable)aadhar_img.getDrawable()).getBitmap()!=null)
        {
            ApgrowPopUpImageView apgrowPopUpImageView = new ApgrowPopUpImageView(this,((BitmapDrawable)aadhar_img.getDrawable()).getBitmap());
            apgrowPopUpImageView.initView();
        }

    }
    @OnClick(R.id.licence_img)
    public void openLiImagePopUp()
    {
        if(((BitmapDrawable)licence_img.getDrawable()).getBitmap()!=null)
        {
            ApgrowPopUpImageView apgrowPopUpImageView = new ApgrowPopUpImageView(this,((BitmapDrawable)licence_img.getDrawable()).getBitmap());
            apgrowPopUpImageView.initView();
        }

    }
    @OnClick(R.id.pan_img)
    public void openPaImagePopUp()
    {
        if(((BitmapDrawable)pan_img.getDrawable()).getBitmap()!=null)
        {
            ApgrowPopUpImageView apgrowPopUpImageView = new ApgrowPopUpImageView(this,((BitmapDrawable)pan_img.getDrawable()).getBitmap());
            apgrowPopUpImageView.initView();
        }

    }

    @Override
    public void onValidationSucceeded() {

        callUpdateAPI();

    }
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
    private void callUpdateAPI() {
        VolleyCallback volleyCallback = new VolleyCallback() {
            @Override
            public void volleyResponse(String string) {
                Log.e(TAG, "volleyResponse: " + string);
                if (!checkIsNullorNot(string)) {
                    parseandSaveLoginAPI(string,UPDATE_CODE);
                } else {
                    showAlertDialog(getString(R.string.alert), getString(R.string.networkerror));
                }

            }
        };
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id",loginData.getUserId());
            jsonObject.put("aadhar_id","");
            jsonObject.put("pan_id","");
            jsonObject.put("dl_id","");
            jsonObject.put("first_name", firstname_edittext.getText().toString());
            jsonObject.put("last_name", lastname_edittext.getText().toString());
            jsonObject.put("middle_name", "");
            jsonObject.put("email", email_edittext.getText().toString());
            jsonObject.put("dob", dob_edittext.getText().toString());
            jsonObject.put("country", country_text.getText().toString());
            jsonObject.put("state", state_text.getText().toString());
            jsonObject.put("address", address_edittext.getText().toString());
            jsonObject.put("district", district_edittext.getText().toString());
            jsonObject.put("mandal", mandal_edittext.getText().toString());
            jsonObject.put("taluk", mandal_edittext.getText().toString());
            jsonObject.put("village", village_edittext.getText().toString());
            if(genderText.getText().toString().equals("Male"))
                jsonObject.put("gender", "1");
            else
                jsonObject.put("gender", "0");
            String url = getString(R.string.base_url) + "registerNewUser";
            VolleyUtilityClass volleyUtilityClass = VolleyUtilityClass.getInstance(ChangeUserInfoFragment.this);
            volleyUtilityClass.callPOSTAPI(url, jsonObject, volleyCallback);
        } catch (Exception e) {

        }
    }
//    uploadImages
private void uploadImagesAPI() {
    VolleyCallback volleyCallback = new VolleyCallback() {
        @Override
        public void volleyResponse(String string) {
            Log.e(TAG, "volleyResponse: " + string);
            if (!checkIsNullorNot(string)) {
                parseandSaveLoginAPI(string, IMAGE_CODE);
            } else {
                showAlertDialog(getString(R.string.alert), getString(R.string.networkerror));
            }
        }
    };
    try {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("user_id","1");

        if(((BitmapDrawable)licence_img.getDrawable()).getBitmap()!=null) {
            JSONObject licenceObj = new JSONObject();

            licenceObj.put("file", covertImgtoBase64(((BitmapDrawable)licence_img.getDrawable()).getBitmap()));
            licenceObj.put("file_name", "dl");
            licenceObj.put("extension", "jpg");
            jsonObject.put("dl",licenceObj);
        }
        if(((BitmapDrawable)aadhar_img.getDrawable()).getBitmap()!=null) {

                JSONObject aadharObj = new JSONObject();
                aadharObj.put("file",  covertImgtoBase64(((BitmapDrawable)aadhar_img.getDrawable()).getBitmap()));
                aadharObj.put("file_name", "aadhar");
                aadharObj.put("extension", "jpg");
                jsonObject.put("aadhar",aadharObj);
            }
        if(((BitmapDrawable)pan_img.getDrawable()).getBitmap()!=null) {
            JSONObject panObj = new JSONObject();
            panObj.put("file",  covertImgtoBase64(((BitmapDrawable)pan_img.getDrawable()).getBitmap()));
            panObj.put("file_name", "pan");
            panObj.put("extension", "jpg");
            jsonObject.put("pan",panObj);
        }




        String url = getString(R.string.base_url) + "uploadImages";
        VolleyUtilityClass volleyUtilityClass = VolleyUtilityClass.getInstance(ChangeUserInfoFragment.this);
        volleyUtilityClass.callPUTAPI(url, jsonObject, volleyCallback);
    } catch (Exception e) {

    }
}
    public String covertImgtoBase64(Bitmap bitmap)
    {
        String encodedString="";
        try
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream); //use the compression format of your need
            InputStream inputStream = new ByteArrayInputStream(stream.toByteArray());
            byte[] bytes;
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bytes = output.toByteArray();
             encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
            // System.out.println(encodedString);
            //Log.e(TAG, "covertImgtoBase64: "+encodedString );

            // Log.e(TAG, "covertImgtoBase64: "+jsonObject );

        }catch (Exception e)
        {
            e.printStackTrace();
        }
return encodedString;
    }


    private void parseandSaveLoginAPI(String response,int code) {

        try {
            Log.d("::RESPONSE::","::"+response);

            if(code==IMAGE_CODE)
            {
                Toast.makeText(this, "Update Succesfull", Toast.LENGTH_SHORT).show();
                uploadImagesAPI();
//                finish();
            }else
            {

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
