package com.agroww;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    AlertDialog alertDialog;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.left_arrow)
    ImageView left_arrow;
    @BindView(R.id.home)
    ImageView home;
    SharedPreferences sharedPreferences;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        ButterKnife.bind(this);
        toolbar_title.setText(getToolbarName());
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
    }

    public SharedPreferences getLocalPreference() {
        return this.getSharedPreferences("loal_preferences", MODE_PRIVATE);
    }

    public void insertValueinPreference(String key, String value) {
        sharedPreferences = getLocalPreference();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
        editor.apply();
    }

    public String getValuefromPreferences(String key) {
        sharedPreferences = getLocalPreference();
        return sharedPreferences.getString(key, "");
    }

    public enum TOPBARVIEWS {
        BACKBUTTON, TITLETEXT, HOMEBUTTON
    }

    public abstract int getLayoutResource();

    public abstract String getToolbarName();

    public void showProgressDialogue() {

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void hideProgressDialogue() {
        progressDialog.dismiss();
    }

    protected void showAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", null);
        builder.setNegativeButton("Cancel", null);
        alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean checkIsNullorNot(String string) {
        if (null != string && !string.equalsIgnoreCase("") && !string.equalsIgnoreCase("volleyerror")) {
            return false;
        } else {
            return true;
        }
    }

    public void setupToolbar(boolean isleft_button, boolean home_button, boolean title_text) {
        setSupportActionBar(toolbar);
        if (!isleft_button)
            left_arrow.setVisibility(View.GONE);
        else
            left_arrow.setVisibility(View.VISIBLE);
        if (!home_button)
            home.setVisibility(View.GONE);
        else
            home.setVisibility(View.VISIBLE);

    }
}
