package com.agroww;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agroww.Requests.LimitInfo;
import com.agroww.Responses.CropVarities.Crops;
import com.agroww.Responses.CropVarities.GetCropResponse;
import com.agroww.Responses.Methods.GetMethodsResponse;
import com.agroww.Responses.Methods.Methods;
import com.agroww.Responses.Seasons.GetSeasonsResponse;
import com.agroww.Responses.Seasons.Seasons;
import com.agroww.dtos.FarmingData;
import com.agroww.localDBUtils.localCrops.LocalCropsFactory;
import com.agroww.localDBUtils.localMethods.LocalMethodsFactory;
import com.agroww.localDBUtils.localSeasons.LocalSeasons;
import com.agroww.localDBUtils.localSeasons.LocalSeasonsFactory;
import com.agroww.networkutils.VolleyCallback;
import com.agroww.networkutils.VolleyUtilityClass;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import apcare.apgrowcomponents.ApgrowPopupView;
import apcare.apgrowcomponents.ICardClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import io.realm.Realm;
import io.realm.RealmResults;

public class AddFarmingActivity extends BaseActivity {

    @BindView(R.id.year_layout)
    RelativeLayout year_layout;
    @BindView(R.id.season_layout)
    RelativeLayout season_layout;
    @BindView(R.id.method_layout)
    RelativeLayout method_layout;
    @BindView(R.id.crop_layout)
    RelativeLayout crop_layout;
    @BindView(R.id.yearText)
    TextView yearText;
    @BindView(R.id.seasonText)
    TextView seasonText;
    @BindView(R.id.activity_add_farming_layout)
    LinearLayout activity_add_farming_layout;
    @BindView(R.id.methodText)
    TextView methodText;
    @BindView(R.id.cropText)
    TextView cropText;
    @BindView(R.id.add_farming)
    Button add_farming;
    private AgrowApp agrowApp;
    private List<FarmingData> farmingDatas;
    private static final String TAG = "AddFarmingActivity";
    private ArrayList<String> seasons, crops, methods;
    private Realm realm;
    private int seasonid, cropid, year, methodid;
    String seasonname, cropname, methodname;
    String description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setupToolbar(true, true, true);
        agrowApp = AgrowApp.getInstance();
        farmingDatas = agrowApp.getFarmingDatas();
        realm = Realm.getDefaultInstance();
        loadCrops();
        loadMethods();
        loadSeasons();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @OnClick(R.id.year_layout)
    public void onYearClick() {
        setupYears();
    }

    @OnTouch(R.id.left_arrow)
    public boolean onBackarrowClicked() {
        finish();
        return true;
    }

    @OnTouch(R.id.home)
    public boolean onHomeClicked() {
        Intent intent = new Intent(this, DashBoardActivity.class);
        startActivity(intent);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @OnClick(R.id.season_layout)
    public void onSeasonClick() {
        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text, int position) {
                Log.e("", "clickedPosition: " + text);
                seasonText.setText(text);
                seasonname = text;
                seasonid = position;
            }
        };
        Log.e(TAG, "onSeasonClick: " + seasons.size());
        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(AddFarmingActivity.this, "Seasons", seasons, iCardClick, activity_add_farming_layout);
        apgrowPopupView.initView();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @OnClick(R.id.method_layout)
    public void onMethodClick() {
        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text, int position) {
                Log.e("", "clickedPosition: " + text);
                methodText.setText(text);
                methodname = text;
                methodid = position;
            }
        };
        Log.e(TAG, "onCropClick: methods" + methods.size());
        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(AddFarmingActivity.this, "Methods", methods, iCardClick, activity_add_farming_layout);
        apgrowPopupView.initView();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @OnClick(R.id.crop_layout)
    public void onCropClick() {
        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text, int position) {
                Log.e("", "clickedPosition: " + text);
                cropText.setText(text);
                cropname = text;
                cropid = position;
            }
        };
        Log.e(TAG, "onCropClick: crop" + crops.size());
        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(AddFarmingActivity.this, "Crops", crops, iCardClick, activity_add_farming_layout);
        apgrowPopupView.initView();
    }

    @OnClick(R.id.add_farming)
    public void onAddtoCropClick() {


        FarmingData farmingData = new FarmingData();
        farmingData.setYear(yearText.getText().toString());
        farmingData.setMethod(methodText.getText().toString());
        farmingData.setCrop(cropText.getText().toString());
        farmingData.setSeason(seasonText.getText().toString());
        farmingDatas.add(farmingData);
        agrowApp.setFarmingDatas((ArrayList<FarmingData>) farmingDatas);
        Intent intent = new Intent(AddFarmingActivity.this, SampleActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("year", year);
        bundle.putInt("seasonid", seasonid);
        bundle.putInt("cropid", cropid);
        bundle.putInt("methodid", methodid);
        bundle.putString("seasonname", seasonname);
        bundle.putString("cropname", cropname);
        bundle.putString("methodname", methodname);
        bundle.putString("description", description);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_add_farming;
    }

    @Override
    public String getToolbarName() {
        return "Add Farming";
    }

    private void loadSeasons() {
        LocalSeasonsFactory localSeasonsFactory = LocalSeasonsFactory.getInstance();
        List<String> localSeasonses = localSeasonsFactory.getSeaonNames(realm);
        if (localSeasonses.size() > 0) {
            seasons = new ArrayList<>(localSeasonses);
        } else {
            showProgressDialogue();
            VolleyCallback seasonsCallback = new VolleyCallback() {
                @Override
                public void volleyResponse(String string) {
                    hideProgressDialogue();
                    if (!checkIsNullorNot(string)) {
                        GetSeasonsResponse getSeasonsResponse = new Gson().fromJson(string, GetSeasonsResponse.class);
                        Log.e(TAG, "volleyResponse: getSeasonsResponse" + getSeasonsResponse.getData().size());
                        LocalSeasonsFactory localSeasonsFactory = LocalSeasonsFactory.getInstance();
                        List<Seasons> seasonsList = getSeasonsResponse.getData();
                        for (Seasons season : seasonsList) {
                            localSeasonsFactory.insert(realm, season);
                        }
                        List<String> localSeasonses = localSeasonsFactory.getSeaonNames(realm);
                        Log.e(TAG, "volleyResponse: localSeasonses" + localSeasonses.size());
                        seasons = new ArrayList<>(localSeasonses);
                    }
                }
            };
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("limit_start", 0);
                jsonObject.put("limit", 50);
                String url = getString(R.string.base_url) + "getSeasons";
                Log.e(TAG, "url " + url);
                VolleyUtilityClass volleyUtilityClass = VolleyUtilityClass.getInstance(AddFarmingActivity.this);
                volleyUtilityClass.callPOSTAPI(url, jsonObject, seasonsCallback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private void loadMethods() {
        LocalMethodsFactory localMethodsFactory = LocalMethodsFactory.getInstance();
        List<String> localMethods = localMethodsFactory.getMethodNames(realm);
        if (localMethods.size() > 0) {
            methods = new ArrayList<>(localMethods);
        } else {
            showProgressDialogue();
            VolleyCallback seasonsCallback = new VolleyCallback() {
                @Override
                public void volleyResponse(String string) {
                    hideProgressDialogue();
                    if (!checkIsNullorNot(string)) {
                        GetMethodsResponse getMethodsResponse = new Gson().fromJson(string, GetMethodsResponse.class);
                        Log.e(TAG, "volleyResponse: getMethodsResponse" + getMethodsResponse.getData().size());
                        LocalMethodsFactory localMethodsFactory = LocalMethodsFactory.getInstance();

                        List<Methods> methodsList = getMethodsResponse.getData();
                        for (Methods method : methodsList) {
                            localMethodsFactory.insert(realm, method);
                        }
                        List<String> localMethods = localMethodsFactory.getMethodNames(realm);
                        Log.e(TAG, "volleyResponse: localMethods" + localMethods.size());
                        methods = new ArrayList<>(localMethods);
                    }
                }
            };
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("limit_start", 0);
                jsonObject.put("limit", 50);
                String url = getString(R.string.base_url) + "getMethods";
                Log.e(TAG, "url " + url);
                VolleyUtilityClass volleyUtilityClass = VolleyUtilityClass.getInstance(AddFarmingActivity.this);
                volleyUtilityClass.callPOSTAPI(url, jsonObject, seasonsCallback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private void loadCrops() {
        LocalCropsFactory localCropsFactory = LocalCropsFactory.getInstance();
        List<String> localcrops = localCropsFactory.getCropNames(realm);
        if (localcrops.size() > 0) {
            crops = new ArrayList<>(localcrops);
        } else {
            showProgressDialogue();
            VolleyCallback seasonsCallback = new VolleyCallback() {
                @Override
                public void volleyResponse(String string) {
                    hideProgressDialogue();
                    if (!checkIsNullorNot(string)) {
                        GetCropResponse getCropResponse = new Gson().fromJson(string, GetCropResponse.class);
                        Log.e(TAG, "volleyResponse: getCropResponse" + getCropResponse.getData().size());
                        LocalCropsFactory localCropsFactory = LocalCropsFactory.getInstance();
                        List<Crops> cropsList = getCropResponse.getData();
                        for (Crops crops : cropsList) {
                            localCropsFactory.insert(realm, crops);
                        }
                        List<String> localCrops = localCropsFactory.getCropNames(realm);
                        Log.e(TAG, "volleyResponse: localCrops" + localCrops.size());
                        crops = new ArrayList<>(localCrops);
                    }
                }
            };
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("limit_start", 0);
                jsonObject.put("limit", 50);
                String url = getString(R.string.base_url) + "getCropVarieties";
                Log.e(TAG, "url " + url);
                VolleyUtilityClass volleyUtilityClass = VolleyUtilityClass.getInstance(AddFarmingActivity.this);
                volleyUtilityClass.callPOSTAPI(url, jsonObject, seasonsCallback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void setupYears() {
        final ArrayList<String> years = new ArrayList<>();
        years.add("2013");
        years.add("2014");
        years.add("2015");
        years.add("2016");
        years.add("2017");

        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String text, int positon) {
                Log.e("", "clickedPosition: " + text);
                yearText.setText(text);
                year = Integer.parseInt(text);
            }
        };

        ApgrowPopupView apgrowPopupView = new ApgrowPopupView(AddFarmingActivity.this, "Years", years, iCardClick, activity_add_farming_layout);
        apgrowPopupView.initView();
    }


    @Override
    public void onBackPressed() {

    }

}
