package com.agroww;

import android.widget.TextView;

/**
 * Created by raghavakumarburugadda on 21/05/17.
 */

public interface IEdittextFields {

    void initFields(int count);
    void setTextView(String text, TextView textView);
}
