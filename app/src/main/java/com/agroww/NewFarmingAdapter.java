package com.agroww;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agroww.Requests.AddUserBalancesheet.UserBalanceSheet;
import com.agroww.Responses.ExpenseItems.ExpenseItem;
import com.agroww.dtos.ChildItem;
import com.agroww.dtos.HeaderItem;
import com.agroww.dtos.NewHeaderItem;
import com.agroww.localDBUtils.localExpenseItems.LocalExpenseItems;
import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;

import java.util.List;

import apcare.apgrowcomponents.FarmingPopupView;
import apcare.apgrowcomponents.IUpdateClick;

/**
 * Created by raghavakumarburugadda on 03/06/17.
 */

public class NewFarmingAdapter extends ExpandableRecyclerAdapter<NewHeaderItem, LocalExpenseItems, HeaderViewHodler, ChildViewHolder> {
    private LayoutInflater mInflater;
    private List<NewHeaderItem> mheaderItems;
    public static final int CHILD_WITHOUTSUBMENU = 1;
    public static final int CHILD_WITHSUBMENU = 2;
    public Context mContext;
    private static final String TAG = "FarmingAdapter";
    private View rootview;
    private static int child_submenu_cost = 0;

    public NewFarmingAdapter(Context context, @NonNull List<NewHeaderItem> parentList, View root_view) {
        super(parentList);
        mheaderItems = parentList;
        mInflater = LayoutInflater.from(context);
        mContext = context;
        rootview = root_view;
    }

    @NonNull
    @Override
    public HeaderViewHodler onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View headerview = mInflater.inflate(R.layout.headerlayout, parentViewGroup, false);
        return new HeaderViewHodler(headerview);
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View childView;
        if (viewType == CHILD_WITHSUBMENU) {
            childView = mInflater.inflate(R.layout.childlayout_withsubmenu, childViewGroup, false);
            ChildWithSubMenu childWithSubMenu = new ChildWithSubMenu(childView);
            return childWithSubMenu;
        } else if (viewType == CHILD_WITHOUTSUBMENU) {
            childView = mInflater.inflate(R.layout.childlayout_withoutsubmenu, childViewGroup, false);
            ChildWithoutSubmenu childWithoutSubmenu = new ChildWithoutSubmenu(childView);
            return childWithoutSubmenu;
        } else {
            childView = mInflater.inflate(R.layout.childlayout_withsubmenu, childViewGroup, false);
            ChildWithoutSubmenu childWithoutSubmenu = new ChildWithoutSubmenu(childView);
            return childWithoutSubmenu;
        }
    }

    @Override
    public void onBindParentViewHolder(@NonNull HeaderViewHodler parentViewHolder, int parentPosition, @NonNull NewHeaderItem parent) {
        parentViewHolder.header_text.setText(parent.getHeadername());
        if (parent.getHeadername().equalsIgnoreCase("Total Expenses") || parent.getHeadername().equalsIgnoreCase("Total Earned")) {
            parentViewHolder.header_layout.setBackgroundResource(R.drawable.rectanglebox);
            parentViewHolder.header_text.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            parentViewHolder.total_cost.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

        } else {
            parentViewHolder.header_layout.setBackgroundResource(R.drawable.expandable_group_background);
            parentViewHolder.header_text.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            parentViewHolder.total_cost.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
        }

        List<LocalExpenseItems> localExpenseItemses = mheaderItems.get(parentPosition).getChildList();
        int total_value = 0;
        int head_total_value = 0;
        if (mheaderItems.get(parentPosition).getHeader_categoryid().equalsIgnoreCase("10")) {

            int cal_value;
            for (int i = 0; i < localExpenseItemses.size(); i++) {
                Log.e(TAG, "onBindParentViewHolder: entered i:" + i);
                Log.e(TAG, "onBindParentViewHolder: localExpenseItemses.size():" + localExpenseItemses.size());
                if (i % 6 == 0) {
                    int revenue = Integer.parseInt(localExpenseItemses.get(i + 2).getSampleData());
                    int cuttings = Integer.parseInt(localExpenseItemses.get(i + 3).getSampleData()) + Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(i + 4).getSampleData()) + Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(i + 5).getSampleData());
                    cal_value = revenue - cuttings;
                    total_value = total_value + cal_value;
                    head_total_value = head_total_value + total_value;
                    total_value = 0;
                }
            }
            mheaderItems.get(parentPosition).setTotalvalue(head_total_value);
            parentViewHolder.total_cost.setText("" + head_total_value);
        } else if (parent.getHeadername().equalsIgnoreCase("Total Expenses") || parent.getHeadername().equalsIgnoreCase("Total Earned")) {
            int sales_value = 0;
            int expenses_value = 0;
            for (int i = 0; i < mheaderItems.size(); i++) {
                if (mheaderItems.get(i).getHeader_categoryid().equalsIgnoreCase("10")) {
                    sales_value = mheaderItems.get(i).getTotalvalue();
                } else {
                    expenses_value = expenses_value + mheaderItems.get(i).getTotalvalue();
                }
            }
            if (parent.getHeadername().equalsIgnoreCase("Total Expenses")) {
                parentViewHolder.total_cost.setText("" + expenses_value);
            } else {
                parentViewHolder.total_cost.setText("" + sales_value);
            }
        } else {
            for (int i = 0; i < localExpenseItemses.size(); i++) {
                total_value = total_value + Integer.parseInt(localExpenseItemses.get(i).getSampleData());
            }
            mheaderItems.get(parentPosition).setTotalvalue(total_value);
            parentViewHolder.total_cost.setText("" + total_value);
        }


    }


    @Override
    public void onBindChildViewHolder(@NonNull ChildViewHolder childViewHolder, final int parentPosition, final int childPosition, @NonNull LocalExpenseItems child) {
        if (childViewHolder instanceof ChildWithSubMenu) {
            ChildWithSubMenu childWithSubMenu = (ChildWithSubMenu) childViewHolder;
            childWithSubMenu.submenu_header.setText(child.getSubmenutext());
            childWithSubMenu.fieldname.setText(child.getExpenseItemName());
            childWithSubMenu.fieldexplanation.setText(child.getDescription());
            childWithSubMenu.value.setTag(childPosition);
            childWithSubMenu.value.setText(child.getSampleData());
            int iteration = mheaderItems.get(parentPosition).getChildList().get(childPosition).getIteration();
            int cal_value = 0;
            Log.e(TAG, "onBindChildViewHolder: childPosition" + childPosition);
            if (mheaderItems.get(parentPosition).getHeader_categoryid().equalsIgnoreCase("9")) {
                iteration = 4;
            } else if (mheaderItems.get(parentPosition).getHeader_categoryid().equalsIgnoreCase("10")) {
                iteration = 6;
            } else {
                iteration = 3;
            }

            if (childPosition % iteration == 0) {
                cal_value = 0;
                if (childPosition == 0) {
                    for (int j = childPosition; j < iteration; j++) {
                        if (mheaderItems.get(parentPosition).getHeader_categoryid().equalsIgnoreCase("10")) {
                            int revenue = Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j + 2).getSampleData());
                            int cuttings = Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j + 3).getSampleData()) + Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j + 4).getSampleData()) + Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j + 5).getSampleData());
                            cal_value = revenue - cuttings;
                            break;
                        } else {
                            cal_value = cal_value + Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j).getSampleData());
                        }
                    }
                    childWithSubMenu.submenu_header_layout.setVisibility(View.VISIBLE);
                    childWithSubMenu.total_sub_cost.setText("" + cal_value);
                } else {
                    cal_value = 0;
                    for (int j = childPosition; j < childPosition + iteration; j++) {
                        Log.e(TAG, "onBindChildViewHolder: entered" + j);
                        if (mheaderItems.get(parentPosition).getHeader_categoryid().equalsIgnoreCase("10")) {
                            int revenue = Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j + 2).getSampleData());
                            int cuttings = Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j + 3).getSampleData()) + Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j + 4).getSampleData()) + Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j + 5).getSampleData());
                            cal_value = revenue - cuttings;
                            break;
                        } else {
                            cal_value = cal_value + Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(j).getSampleData());
                        }

                    }
                    childWithSubMenu.submenu_header_layout.setVisibility(View.VISIBLE);
                    childWithSubMenu.total_sub_cost.setText("" + cal_value);
                }
            } else {
                childWithSubMenu.submenu_header_layout.setVisibility(View.GONE);
            }
            if (mheaderItems.get(parentPosition).getHeader_categoryid().equalsIgnoreCase("10")) {
                if (mheaderItems.get(parentPosition).getChildList().get(childPosition).getExpenseItemId().equalsIgnoreCase("33")) {
                    int sold_qty = Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(childPosition - 2).getSampleData());
                    int per_unit = Integer.parseInt(mheaderItems.get(parentPosition).getChildList().get(childPosition - 1).getSampleData());
                    mheaderItems.get(parentPosition).getChildList().get(childPosition).setSampleData("" + (sold_qty * per_unit));
                    childWithSubMenu.value.setText("" + (sold_qty * per_unit));
                    childWithSubMenu.value.setEnabled(false);
                } else {
                    childWithSubMenu.value.setEnabled(true);
                }
            }

            childWithSubMenu.value.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                @Override
                public void onClick(View view) {

                    Log.e(TAG, "onClick: dkfjdks");

                    IUpdateClick iUpdateClick = new IUpdateClick() {
                        @Override
                        public void getDateandValue(String date, String value) {
                            Log.e(TAG, "getDateandValue: date:" + date + "value:" + value);
                            mheaderItems.get(parentPosition).getChildList().get(childPosition).setSampleData(value);
                            notifyChildChanged(parentPosition, childPosition);
                            notifyDataSetChanged();
                            addtoBalanceSheet(mheaderItems.get(parentPosition).getChildList().get(childPosition), date);


                        }
                    };
                    FarmingPopupView farmingPopupView = new FarmingPopupView(mContext, "Title", iUpdateClick, rootview);
                    farmingPopupView.initView();
                }
            });
        } else {
            {
                final ChildWithoutSubmenu childWithSubMenu = (ChildWithoutSubmenu) childViewHolder;
                childWithSubMenu.fieldname.setText(child.getExpenseItemName());
                childWithSubMenu.fieldexplanation.setText(child.getDescription());
                childWithSubMenu.value.setTag(childPosition);
                childWithSubMenu.value.setText("" + mheaderItems.get(parentPosition).getChildList().get(childPosition).getSampleData());
                childWithSubMenu.value.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                    @Override
                    public void onClick(View view) {

                        Log.e(TAG, "onClick: child with submenu");
                        IUpdateClick iUpdateClick1 = new IUpdateClick() {
                            @Override
                            public void getDateandValue(String date, String value) {
                                Log.e(TAG, "getDateandValue: date:" + date + "value:" + value);
                                mheaderItems.get(parentPosition).getChildList().get(childPosition).setSampleData(value);
                                childWithSubMenu.value.setText("" + mheaderItems.get(parentPosition).getChildList().get(childPosition).getSampleData());
                                notifyChildChanged(parentPosition, childPosition);
                                notifyDataSetChanged();
                                addtoBalanceSheet(mheaderItems.get(parentPosition).getChildList().get(childPosition), date);
                            }
                        };
                        FarmingPopupView farmingPopupView = new FarmingPopupView(mContext, "Title", iUpdateClick1, rootview);
                        farmingPopupView.initView();
                    }
                });
            }
        }
    }

    public void addtoBalanceSheet(LocalExpenseItems localExpenseItems, String date) {
        UserBalanceSheet userBalanceSheet = new UserBalanceSheet();
        userBalanceSheet.setExpenseCatMstId(Integer.parseInt(localExpenseItems.getExpenseCategoryId()));
        userBalanceSheet.setExpenseItemId(Integer.parseInt(localExpenseItems.getExpenseItemId()));
        userBalanceSheet.setExpenseItemVal(Integer.parseInt(localExpenseItems.getSampleData()));
        userBalanceSheet.setExpenseDate(date);
        userBalanceSheet.setIteration(localExpenseItems.getIteration());
        SampleActivity.balanceSheet.add(userBalanceSheet);
    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {
        LocalExpenseItems expenseItem = mheaderItems.get(parentPosition).getChildList().get(childPosition);
        if (expenseItem.getSubmenutext().equalsIgnoreCase("none")) {
            return CHILD_WITHOUTSUBMENU;
        } else {
            return CHILD_WITHSUBMENU;
        }
    }

}

