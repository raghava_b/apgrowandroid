package com.agroww;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.agroww.Requests.AddUserBalancesheet.AddUserBalanceSheetRequest;
import com.agroww.Requests.AddUserBalancesheet.UserBalanceSheet;
import com.agroww.Responses.ExpenseItems.ExpenseItem;
import com.agroww.Responses.ExpenseItems.GetExpenseItemsResponse;
import com.agroww.dtos.ChildItem;
import com.agroww.dtos.Example;
import com.agroww.dtos.HeaderItem;
import com.agroww.dtos.NewHeaderItem;
import com.agroww.localDBUtils.localExpenseItems.ExpenseItemFactory;
import com.agroww.networkutils.VolleyCallback;
import com.agroww.networkutils.VolleyUtilityClass;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import apcare.apgrowcomponents.ApgrowPopupView;
import apcare.apgrowcomponents.ICardClick;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import io.realm.Realm;


public class SampleActivity extends BaseActivity {
    @BindView(R.id.expandable_list)
    RecyclerView expandable_list;
    Example data;
    List<HeaderItem> listData;
    HeaderItem headerItem;
    FarmingAdapter farmingAdapter;
    NewFarmingAdapter newFarmingAdapter;
    private static final String TAG = "SampleActivity";
    @BindView(R.id.activity_sample_rootview)
    RelativeLayout activity_sample_rootview;
    @BindView(R.id.savemyBalanceSheet)
    Button savemyBalanceSheet;

    List<ChildItem> childItems = new ArrayList<>();
    Realm realm;
    public static List<UserBalanceSheet> balanceSheet = new ArrayList<>();
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setupToolbar(true, true, true);
        //prepareData();
        realm = Realm.getDefaultInstance();
        loadData();
        bundle = getIntent().getExtras();
        Log.e(TAG, "onCreate: " + bundle);
    }

    @OnClick(R.id.savemyBalanceSheet)
    public void onSaveClicked() {
        Log.e(TAG, "onSaveClicked: size" + balanceSheet.size());
        AddUserBalanceSheetRequest addUserBalanceSheetRequest = new AddUserBalanceSheetRequest();
        addUserBalanceSheetRequest.setUserId(1);
        addUserBalanceSheetRequest.setYear(bundle.getInt("year"));
        addUserBalanceSheetRequest.setCropVarietyId(bundle.getInt("cropid"));
        addUserBalanceSheetRequest.setSeasonId(bundle.getInt("seasonid"));
        addUserBalanceSheetRequest.setMethodId(bundle.getInt("methodid"));
        addUserBalanceSheetRequest.setCropVarietyName(bundle.getString("seasonname"));
        addUserBalanceSheetRequest.setCropVarietyName(bundle.getString("cropname"));
        addUserBalanceSheetRequest.setCropVarietyName(bundle.getString("methodname"));
        addUserBalanceSheetRequest.setDescription(bundle.getString("description"));
        addUserBalanceSheetRequest.setBalanceSheet(balanceSheet);

        String str = new Gson().toJson(addUserBalanceSheetRequest);

        try {
            showProgressDialogue();
            VolleyCallback volleyCallback = new VolleyCallback() {
                @Override
                public void volleyResponse(String string) {
                    Log.e(TAG, "volleyResponse: " + string);
                    hideProgressDialogue();
                }
            };
            JSONObject jsonObject = new JSONObject(str);
            System.out.println(jsonObject);
            String url = getString(R.string.base_url) + "addUserBalanceSheet";
            Log.e(TAG, "url " + url);
            VolleyUtilityClass volleyUtilityClass = VolleyUtilityClass.getInstance(SampleActivity.this);
            volleyUtilityClass.callPOSTAPI(url, jsonObject, volleyCallback);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @OnTouch(R.id.left_arrow)
    public boolean onBackarrowClicked() {
        finish();
        return true;
    }

    @OnTouch(R.id.home)
    public boolean onHomeClicked() {
        Intent intent = new Intent(this, DashBoardActivity.class);
        startActivity(intent);
        return true;
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_sample;
    }

    @Override
    public String getToolbarName() {
        return "Farming";
    }

    private void loadData() {
        ExpenseItemFactory expenseItemFactory = ExpenseItemFactory.getInstance();
        if (expenseItemFactory.getTotalExpenses(realm).size() > 0) {
            List<NewHeaderItem> listData = expenseItemFactory.getExpandableData(realm);
            Log.e(TAG, "volleyResponse: getexpandabledata" + listData.size());
            Log.e(TAG, "loadData: child list" + listData.get(0).getChildList());
            newFarmingAdapter = new NewFarmingAdapter(SampleActivity.this, listData, activity_sample_rootview);
            expandable_list.setLayoutManager(new LinearLayoutManager(this));
            expandable_list.setAdapter(newFarmingAdapter);

        } else {
            showProgressDialogue();
            VolleyCallback expensesCallback = new VolleyCallback() {
                @Override
                public void volleyResponse(String string) {
                    hideProgressDialogue();
                    System.out.println("response:" + string);
                    if (!checkIsNullorNot(string)) {
                        GetExpenseItemsResponse getExpenseItemsResponse = new Gson().fromJson(string, GetExpenseItemsResponse.class);
                        Log.e(TAG, "expensesResponse: getExpenseItemsResponse size" + getExpenseItemsResponse.getData().size());
                        ExpenseItemFactory expenseItemFactory = ExpenseItemFactory.getInstance();
                        for (ExpenseItem expenseItem : getExpenseItemsResponse.getData()) {
                            expenseItemFactory.insert(realm, expenseItem);
                        }
                        List<NewHeaderItem> listData = expenseItemFactory.getExpandableData(realm);
                        Log.e(TAG, "volleyResponse: getexpandabledata" + listData.size());
                        newFarmingAdapter = new NewFarmingAdapter(SampleActivity.this, listData, activity_sample_rootview);
                        expandable_list.setLayoutManager(new LinearLayoutManager(SampleActivity.this));
                        expandable_list.setAdapter(newFarmingAdapter);
                    }
                }
            };
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("limit_start", 0);
                jsonObject.put("limit", 50);
                String url = getString(R.string.base_url) + "getExpenseItems";
                Log.e(TAG, "url " + url);
                VolleyUtilityClass volleyUtilityClass = VolleyUtilityClass.getInstance(SampleActivity.this);
                volleyUtilityClass.callPOSTAPI(url, jsonObject, expensesCallback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void prepareData() {
        String response = loadJSONFromAsset();
        System.out.println(response);
        data = new Gson().fromJson(response, Example.class);
        listData = new ArrayList<>();
        listData.add(prepareandGetHeaderItem("Land Preparation", data.getLandpreparation()));
        listData.add(prepareandGetHeaderItem("Seeds/sowing", data.getSeedssowing()));
        listData.add(prepareandGetHeaderItem("Fertilizers", data.getFertilizers()));
        listData.add(prepareandGetHeaderItem("Pesticides", data.getPesticides()));
        listData.add(prepareandGetHeaderItem("Weeding", data.getWeeding()));
        listData.add(prepareandGetHeaderItem("Irrigation", data.getIrrigation()));
        listData.add(prepareandGetHeaderItem("Harvesting", data.getHarvesting()));
        listData.add(prepareandGetHeaderItem("Storage", data.getStorage()));
        listData.add(prepareandGetHeaderItem("Processing", data.getProcessing()));
        listData.add(prepareandGetHeaderItem("Selling", data.getSelling()));
        listData.add(prepareandGetHeaderItem("Others", data.getOthers()));
        listData.add(prepareandGetHeaderItem("Total Expenses", childItems));
        listData.add(prepareandGetHeaderItem("Total Earned", childItems));
        Log.e(TAG, "prepareData: listdata" + listData.size());
        farmingAdapter = new FarmingAdapter(SampleActivity.this, listData, activity_sample_rootview);
        expandable_list.setLayoutManager(new LinearLayoutManager(this));
        expandable_list.setAdapter(farmingAdapter);

    }

    @Override
    public void onBackPressed() {

    }


    private HeaderItem prepareandGetHeaderItem(String title, List<ChildItem> childItems) {
        headerItem = new HeaderItem();
        headerItem.setHeadername(title);
        headerItem.setTotalvalue("0");
        headerItem.setChildItems(childItems);
        return headerItem;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = SampleActivity.this.getAssets().open("sample.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
