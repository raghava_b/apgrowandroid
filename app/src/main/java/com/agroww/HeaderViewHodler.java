package com.agroww;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;

/**
 * Created by raghavakumarburugadda on 27/05/17.
 */

public class HeaderViewHodler extends ParentViewHolder {

    TextView header_text, total_cost;
    RelativeLayout header_layout;

    public HeaderViewHodler(View itemView) {
        super(itemView);
        header_text=(TextView)itemView.findViewById(R.id.header_text);
        total_cost=(TextView)itemView.findViewById(R.id.total_cost);
        header_layout=(RelativeLayout)itemView.findViewById(R.id.header_layout);
    }
}
