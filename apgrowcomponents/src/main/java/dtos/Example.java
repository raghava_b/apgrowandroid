
package dtos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example {

    @SerializedName("landpreparation")
    @Expose
    private List<ChildItem> landpreparation = null;
    @SerializedName("seedssowing")
    @Expose
    private List<ChildItem> seedssowing = null;
    @SerializedName("fertilizers")
    @Expose
    private List<ChildItem> fertilizers = null;
    @SerializedName("pesticides")
    @Expose
    private List<ChildItem> pesticides = null;
    @SerializedName("weeding")
    @Expose
    private List<ChildItem> weeding = null;
    @SerializedName("irrigation")
    @Expose
    private List<ChildItem> irrigation = null;
    @SerializedName("harvesting")
    @Expose
    private List<ChildItem> harvesting = null;
    @SerializedName("storage")
    @Expose
    private List<ChildItem> storage = null;
    @SerializedName("processing")
    @Expose
    private List<ChildItem> processing = null;
    @SerializedName("selling")
    @Expose
    private List<ChildItem> selling = null;
    @SerializedName("others")
    @Expose
    private List<ChildItem> others = null;

    public List<ChildItem> getLandpreparation() {
        return landpreparation;
    }

    public void setLandpreparation(List<ChildItem> landpreparation) {
        this.landpreparation = landpreparation;
    }

    public List<ChildItem> getSeedssowing() {
        return seedssowing;
    }

    public void setSeedssowing(List<ChildItem> seedssowing) {
        this.seedssowing = seedssowing;
    }

    public List<ChildItem> getFertilizers() {
        return fertilizers;
    }

    public void setFertilizers(List<ChildItem> fertilizers) {
        this.fertilizers = fertilizers;
    }

    public List<ChildItem> getPesticides() {
        return pesticides;
    }

    public void setPesticides(List<ChildItem> pesticides) {
        this.pesticides = pesticides;
    }

    public List<ChildItem> getWeeding() {
        return weeding;
    }

    public void setWeeding(List<ChildItem> weeding) {
        this.weeding = weeding;
    }

    public List<ChildItem> getIrrigation() {
        return irrigation;
    }

    public void setIrrigation(List<ChildItem> irrigation) {
        this.irrigation = irrigation;
    }

    public List<ChildItem> getHarvesting() {
        return harvesting;
    }

    public void setHarvesting(List<ChildItem> harvesting) {
        this.harvesting = harvesting;
    }

    public List<ChildItem> getStorage() {
        return storage;
    }

    public void setStorage(List<ChildItem> storage) {
        this.storage = storage;
    }

    public List<ChildItem> getProcessing() {
        return processing;
    }

    public void setProcessing(List<ChildItem> processing) {
        this.processing = processing;
    }

    public List<ChildItem> getSelling() {
        return selling;
    }

    public void setSelling(List<ChildItem> selling) {
        this.selling = selling;
    }

    public List<ChildItem> getOthers() {
        return others;
    }

    public void setOthers(List<ChildItem> others) {
        this.others = others;
    }

}
