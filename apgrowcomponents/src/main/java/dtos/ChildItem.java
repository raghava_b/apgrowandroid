package dtos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by raghavakumarburugadda on 27/05/17.
 */

public class ChildItem {
    int view_type = 1;

    public int getView_type() {
        return view_type;
    }

    public void setView_type(int view_type) {
        this.view_type = view_type;
    }

    @SerializedName("fieldame")
    @Expose
    private String fieldame;
    @SerializedName("explanation")
    @Expose
    private String explanation;
    @SerializedName("exampledata")
    @Expose
    private String exampledata;
    @SerializedName("submenu")
    @Expose
    private String submenu;

    public String getFieldame() {
        return fieldame;
    }

    public void setFieldame(String fieldame) {
        this.fieldame = fieldame;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getExampledata() {
        return exampledata;
    }

    public void setExampledata(String exampledata) {
        this.exampledata = exampledata;
    }

    public String getSubmenu() {
        return submenu;
    }

    public void setSubmenu(String submenu) {
        this.submenu = submenu;
    }

}
