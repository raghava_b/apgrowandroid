package dtos;

/**
 * Created by raghavakumarburugadda on 27/05/17.
 */

public class HeaderItem {
    int view_type;
    String headername;
    String totalvalue;

    public int getView_type() {
        return view_type;
    }

    public void setView_type(int view_type) {
        this.view_type = view_type;
    }

    public String getHeadername() {
        return headername;
    }

    public void setHeadername(String headername) {
        this.headername = headername;
    }

    public String getTotalvalue() {
        return totalvalue;
    }

    public void setTotalvalue(String totalvalue) {
        this.totalvalue = totalvalue;
    }
}
