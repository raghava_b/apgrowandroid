package apcare.apgrowcomponents;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Manjusha on 6/6/2017.
 */
public class ApgrowPopUpImageView {
    private Context mContext;
    private  Bitmap bitmap;
    private Dialog popupWindow;

    public ApgrowPopUpImageView(Context mContext, Bitmap bitmap) {
        this.mContext = mContext;
        this.bitmap = bitmap;
    }

    public void initView() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_image, null);
        popupWindow = new Dialog(mContext, R.style.FullHeightDialog);
        popupWindow.setContentView(view);


        ImageView close_button = (ImageView) view.findViewById(R.id.close_button);
        ImageView ivPopup = (ImageView) view.findViewById(R.id.ivPopup);

        close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        ivPopup.setImageBitmap(bitmap);
        popupWindow.setCancelable(true); // can dismiss alert screen when user click back buttonon
        popupWindow.setCanceledOnTouchOutside(false);
        popupWindow.show();
    }

}
