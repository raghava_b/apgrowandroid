package apcare.apgrowcomponents;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by raghavakumarburugadda on 20/05/17.
 */

public class ApgrowButton extends Button{
    public ApgrowButton(Context context) {
        super(context);
        init(context);
    }

    public ApgrowButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ApgrowButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ApgrowButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context context)
    {
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "helvetica_bold.ttf");
        setTypeface(custom_font);
    }
}
