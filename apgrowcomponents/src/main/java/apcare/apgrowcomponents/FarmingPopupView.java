package apcare.apgrowcomponents;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * Created by raghavakumarburugadda on 28/05/17.
 */

public class FarmingPopupView {

    private Context mContext;
    private PopupWindow popupWindow;
    private View root_view;
    private IUpdateClick iUpdateClick;
    private String popup_title;
    private LayoutInflater layoutInflater;

    public FarmingPopupView(Context _context, String title, IUpdateClick _iUpdateClick, View _root_view) {
        mContext = _context;
        iUpdateClick = _iUpdateClick;
        root_view = _root_view;
        popup_title = title;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void initView() {
        View view = layoutInflater.inflate(R.layout.farming_popuplayout, null);
        popupWindow = new PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true
        );

        applyDim((ViewGroup) root_view, 0.5f);

        TextView popup_text = (TextView) view.findViewById(R.id.popup_text);
        ImageView close_button = (ImageView) view.findViewById(R.id.close_button);
        final EditText type_value = (EditText) view.findViewById(R.id.type_value);
        final EditText dob_edittext = (EditText) view.findViewById(R.id.dob_edittext);
        Button update = (Button) view.findViewById(R.id.update);
        popup_text.setText(popup_title);
        close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                clearDim((ViewGroup) root_view);
            }
        });
        DatePickerUtils datePickerUtils = new DatePickerUtils(dob_edittext, mContext);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iUpdateClick.getDateandValue(dob_edittext.getText().toString(), type_value.getText().toString());
                popupWindow.dismiss();
                clearDim((ViewGroup) root_view);
            }
        });
        popupWindow.showAtLocation(root_view, Gravity.CENTER, 0, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static void applyDim(@NonNull ViewGroup parent, float dimAmount) {
        Drawable dim = new ColorDrawable(Color.BLACK);
        dim.setBounds(0, 0, parent.getWidth(), parent.getHeight());
        dim.setAlpha((int) (255 * dimAmount));

        ViewGroupOverlay overlay = parent.getOverlay();
        overlay.add(dim);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static void clearDim(@NonNull ViewGroup parent) {
        ViewGroupOverlay overlay = parent.getOverlay();
        overlay.clear();
    }
}
