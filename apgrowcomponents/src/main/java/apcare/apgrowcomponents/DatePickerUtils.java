package apcare.apgrowcomponents;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by raghavakumarburugadda on 22/05/17.
 */

public class DatePickerUtils implements  DatePickerDialog.OnDateSetListener, View.OnTouchListener {

    private EditText editText;
    private Context context;
    private Calendar myCalendar;

    public DatePickerUtils(EditText date_view, Context context) {
        this.editText = date_view;
        this.context = context;
        myCalendar = Calendar.getInstance();
//        date_view.setOnClickListener(this);
//        date_view.setOnFocusChangeListener(this);
//        editText.setFocusableInTouchMode(true);
        date_view.setOnTouchListener(this);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        editText.setText(sdformat.format(myCalendar.getTime()));

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            DatePickerDialog datePickerDialog=  new DatePickerDialog(context, this, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
        return false;
    }
}
