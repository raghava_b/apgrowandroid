package apcare.apgrowcomponents;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by raghavakumarburugadda on 20/05/17.
 */

public class ApgrowPopupView {

    private Context mContext;
    private ArrayList<String> data;
    private String popuptext;
    private Dialog popupWindow;
    private ICardClick iCardClick;
    private View root_view;

    public ApgrowPopupView(Context context, String popup_text, ArrayList<String> _data, ICardClick cardClick,View rootView) {
        mContext = context;
        data = _data;
        popuptext = popup_text;
        root_view=rootView;
        iCardClick=cardClick;
    }

    public void initView() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popuplayout, null);
        popupWindow = new Dialog(mContext, R.style.FullHeightDialog);
        popupWindow.setContentView(view);


        TextView popup_text = (TextView) view.findViewById(R.id.popup_text);
        ImageView close_button = (ImageView) view.findViewById(R.id.close_button);
        RecyclerView popupList=(RecyclerView)view.findViewById(R.id.popupList);
        close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        RecyclerAdapter recyclerAdapter=new RecyclerAdapter();
        popupList.setLayoutManager(new LinearLayoutManager(mContext));
        popupList.setAdapter(recyclerAdapter);
        popup_text.setText(popuptext);
        popupWindow.setCancelable(true); // can dismiss alert screen when user click back buttonon
        popupWindow.setCanceledOnTouchOutside(false);
        popupWindow.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static void applyDim(@NonNull ViewGroup parent, float dimAmount) {
        Drawable dim = new ColorDrawable(Color.BLACK);
        dim.setBounds(0, 0, parent.getWidth(), parent.getHeight());
        dim.setAlpha((int) (255 * dimAmount));

        ViewGroupOverlay overlay = parent.getOverlay();
        overlay.add(dim);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static void clearDim(@NonNull ViewGroup parent) {
        ViewGroupOverlay overlay = parent.getOverlay();
        overlay.clear();
    }

    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerHolder> {


        class RecyclerHolder extends RecyclerView.ViewHolder {

            TextView popuplist_text;
            CardView listitem_card;
            RelativeLayout text_layout;

            public RecyclerHolder(View itemView) {
                super(itemView);
                popuplist_text = (TextView) itemView.findViewById(R.id.popuplist_text);
                listitem_card=(CardView)itemView.findViewById(R.id.listitem_card);
                text_layout=(RelativeLayout)itemView.findViewById(R.id.text_layout);
            }
        }

        @Override
        public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            View item_view = layoutInflater.inflate(R.layout.popupviewtext, null);
            RecyclerHolder recyclerHolder = new RecyclerHolder(item_view);
            return recyclerHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerHolder holder, final int position) {
            holder.popuplist_text.setText(data.get(position));
            holder.listitem_card.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                @Override
                public void onClick(View view) {

                    popupWindow.dismiss();

                    clearDim((ViewGroup) root_view);
                    iCardClick.clickedPosition(data.get(position),position);

                   clearDim((ViewGroup) root_view);
                    iCardClick.clickedPosition(data.get(position),position);

                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }
}
