package apcare.apgrowcomponents;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by raghavakumarburugadda on 17/05/17.
 */

public class ApgrowEditText extends EditText{
    public ApgrowEditText(Context context) {
        super(context);
        init(context);
    }

    public ApgrowEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ApgrowEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ApgrowEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }
    public void init(Context context)
    {
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "helvetica_light.ttf");
        setTypeface(custom_font);
    }
}
