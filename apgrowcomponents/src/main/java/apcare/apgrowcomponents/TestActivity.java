package apcare.apgrowcomponents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {

    LinearLayout dropdown1,dropdown2;
    RelativeLayout spinner_layout,spinner_layout1;
    private static final String TAG = "TestActivity";
    LinearLayout root_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dropdown1 = (LinearLayout) findViewById(R.id.dropdown1);
        dropdown2 = (LinearLayout) findViewById(R.id.dropdown2);
        root_view=(LinearLayout)findViewById(R.id.root_layout) ;
        spinner_layout=(RelativeLayout)dropdown1.findViewById(R.id.spinner_layout);
        spinner_layout1=(RelativeLayout)dropdown1.findViewById(R.id.spinner_layout);

        final ArrayList<String> countries = new ArrayList<>();
        countries.add("INDIA");
        countries.add("USA");
        countries.add("UK");
        countries.add("SPAIN");
        countries.add("ISTAMBUL");
        countries.add("TURKEY");


        final ArrayList<String> cities = new ArrayList<>();
        cities.add("HYD");
        cities.add("MUMBAI");
        cities.add("DELHI");
        cities.add("BANGLORE");

        final ICardClick iCardClick = new ICardClick() {
            @Override
            public void clickedPosition(String position,int postion) {
                Log.e("", "clickedPosition: "+position );
            }
        };

        spinner_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: spinner");
                ApgrowPopupView apgrowPopupView = new ApgrowPopupView(TestActivity.this, "Countries", countries, iCardClick,root_view);
                apgrowPopupView.initView();
            }
        });


        final ICardClick iCardClick1 = new ICardClick() {
            @Override
            public void clickedPosition(String position,int pos) {
                Log.e("", "clickedPosition: "+position );
            }
        };
        dropdown1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("", "onClick:");

            }
        });

    }
}
