package apcare.apgrowcomponents;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by raghavakumarburugadda on 17/05/17.
 */

public class ApgrowBoldTextView extends TextView {
    public ApgrowBoldTextView(Context context) {
        super(context);
        init(context);
    }

    public ApgrowBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ApgrowBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ApgrowBoldTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context context) {
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "helvetica_bold.ttf");
        setTypeface(custom_font);
    }
}
